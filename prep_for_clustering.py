import pandas as pd
import numpy as np
pd.set_option('display.width', 180)

### Choose version
### and take the dmas with the highest purchasing per person, enough to have this fraction of the total # ppl
# vers = 'Top25per'
# subsamp_frac = 0.25
# target = full_samp_size * subsamp_frac
# target = 15000 # actual number will be < 2/3 of this

vers='EasternStates'
statelist=['South Carolina', 'North Carolina', 'Virginia', 'New Jersey', 'Pennsylvania',
           'Connecticut', 'Massachusetts', 'Rhode Island', 'New York']

# vers = 'Program_all'
# subsamp_frac = 1.

### 4703 of 11376 meet 10 of 12 months criterion
purchasing = pd.read_csv('data/purch_data.csv')
purchasing['month'] = [s[2:5] for s in purchasing.CAL_DT]
hh_n_months = purchasing[['PANL_HHLD_ID_NBR','month']].drop_duplicates().groupby('PANL_HHLD_ID_NBR').month.count()
purchasing['n_mnths'] = list(hh_n_months.loc[purchasing['PANL_HHLD_ID_NBR']])

sttn_genre = pd.read_csv('data/StationGenreDescriptions.csv', index_col = 0)

# print('Clustering {0} viewers, {1} of total sample'.format(vers, subsamp_frac))
ppl_all = pd.read_csv('data/person_data.csv', index_col = 0)

################ Choose subsampling system ###################################
### based on hh purchasing in a tleast 10 of 12 months
# ok_hhs = hh_n_months.loc[hh_n_months >= 10].index
# print('Using {0} of {1} households'.format(ok_hhs.shape[0], hh_n_months.shape[0]))
# ppl = ppl_all.loc[ppl_all['Household Number'].isin(ok_hhs)].copy()
# print('Using {0} of {1} people'.format(ppl.shape[0], ppl_all.shape[0]))
# ---------------------------------------------------------------------------- #
### Subsample by dma?
from dma_df import *
full_samp_size = dma_purch_covg['NUM OF PERSONS'].sum()
# full_samp_size = purch_hh_dma.n_viewers.sum()

if vers=='Top25per':
    n=1
    while dma_purch_covg.iloc[:n]['NUM OF PERSONS'].sum() < target:
        n += 1
    dma_list = dma_purch_covg.iloc[:n].index
elif vers == 'EasternStates':
    dma_list = dma_stats.loc[dma_stats.state.isin(statelist) & (dma_stats.coverage_10mo >= 0.4)].index
else:
    assert 1==0, 'ERROR: invalid version "{0}"'.format(vers)
### Get ppl from those dmas
ppl = ppl_all.loc[ppl_all['DMA CODE'].isin(dma_list)].copy()
print('Using {0} of {1} people'.format(ppl.shape[0], ppl_all.shape[0]))
################################################################################

### Identify different types of columns
daypart_cols = ['night','morning','afternoon','evening']
daypart_frac_cols = ['frac_'+c for c in daypart_cols]
weekday_cols = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']
weekday_frac_cols = ['frac_'+c for c in weekday_cols]
genre_cols = [c for c in ppl.columns if c[:6] == 'genre_']
genre_cols = [c for c in genre_cols if '_frac' not in c]
genre_cols = list(ppl[[c for c in ppl.columns if c[:6] == 'genre_']].sum().sort_values(ascending=False).index)
genre_frac_cols   = ['frac_'+c for c in genre_cols]
show_cols  = [c for c in ppl.columns if c[:2] == 'SH']
movie_cols = [c for c in ppl.columns if c[:2] == 'MV']
sp_cols    = [c for c in ppl.columns if c[:2] == 'SP']
program_cols = show_cols + movie_cols + sp_cols
program_frac_cols = ['frac_'+c for c in program_cols]
tv_agg_cols = ['n_mins'] + daypart_frac_cols + weekday_frac_cols
tv_cols     = tv_agg_cols + genre_frac_cols #+ program_cols
###
program_info = pd.read_csv('data/show_info.csv', index_col = 0)[['program_id','program_name','show_type']]\
                 .drop_duplicates().set_index('program_id')
program_info['n_mins'] = ppl[program_cols].sum()
program_info = program_info.sort_values('n_mins',ascending=False)
### Add rows for the non-TVshow genres
for g in ['genre_999', 'genre_919', 'genre_417']:
    program_info.loc[g] = [sttn_genre.loc[g.replace('genre_', ''), 'description'], 'other', ppl[g].sum()]
### Get rid of the one NaN row
program_info = program_info.loc[pd.notnull(program_info.index)]
assert program_info.isnull().sum().sum() == 0, 'ERROR: program_info contains NaNs'

### Purchasing data
purch_cols  = [c for c in ppl.columns if 'buy_' in c]
ppl['hh_spent'] = ppl[purch_cols].sum(axis=1)
dept_totals = ppl[purch_cols].sum().sort_values(ascending=False)
buy_cols = list(dept_totals.index)
frac_buy_cols = ['frac_'+c for c in buy_cols]
ppl[frac_buy_cols] = ppl[buy_cols].divide(ppl[buy_cols].sum(axis=1), axis=0)

### Make numeric demographic columns
brth_century = {9: 1800,
                0: 1900,
                1: 2000}
race_dict = {1:  'White',
             2:  'Black',
             3:  'Japanese',
             4:  'Chinese',
             5:  'Filipino',
             6:  'Korean',
             7:  'Vietnamese',
             8:  'American Indian',
             9:  'Asian Indian',
             10: 'Hawaiian',
             11: 'Other',
             99: 'Unknown'}
lang_dict   = {0: 0,
               1: 1,
               2: 0,
               3: 0.75,
               4: 0.25,
               5: 0.5,
               6: 0}
origin_dict = {1: 'Non-Spanish',
               2: 'Spanish',
               9: 'Unknown'}
### Already numeric?
demos = ppl[['INCOME AMT','HOH EDUCATION CODE','TIME ZONE CODE','Education']].copy()
### Convert weird Nielsen things
### Age as of Jan 1, 2017
demos['age'] = 2017 - (pd.Series([brth_century[i] for i in ppl['Century of Birth']], index = ppl.index) + \
                       pd.Series([int(s[:2]) for s in ppl['Birth Date'].astype('str').str.zfill(4)], index = ppl.index))
demos['working_hours'] = ppl['Number of Working Hours']
demos['is_male'] = ppl['Sex'] == 'M'
demos['amt_spanish_spoken'] = [lang_dict[i] for i in ppl["Person's Language"]]
demos['spanish_origin']     = ppl["Person's Origin"] == 2
for i in ppl['Race'].value_counts().index:
    demos['is_'+race_dict[i]] = ppl['Race'] == i
# for c in ['METRO CNTY IND','BOTTLED WATER IND','COFFEE TEA IND','SOFT DRINKS IND','TABLE WINE IND']:
#     demos[c] = (ppl[c] == 'Y')
demos['metro']      = ppl[   'METRO CNTY IND'] == 'Y'
demos['soda']       = ppl[  'SOFT DRINKS IND'] == 'Y'
demos['btld_water'] = ppl['BOTTLED WATER IND'] == 'Y'
demos['coffee_tea'] = ppl[   'COFFEE TEA IND'] == 'Y'
demos['wine']       = ppl[   'TABLE WINE IND'] == 'Y'
demos['has_dog']    = ppl['NUM OF DOGS'] > 0
demos['has_cat']    = ppl['NUM OF CATS'] > 0
### Household ages
# for c in ['NUM OF ADULTS', 'NUM OF CHILDREN UNDER 18', 'NUM OF CHILDREN UNDER 12', 'NUM OF CHILDREN UNDER 3']:
#     demos[c] = ppl[c]
demos['n_adults'] = ppl['NUM OF ADULTS']
demos['n_12-18']  = ppl['NUM OF CHILDREN UNDER 18'] - ppl['NUM OF CHILDREN UNDER 12']
demos['n_6-12']   = ppl['NUM OF CHILDREN UNDER 12'] - ppl['NUM OF CHILDREN UNDER 6']
demos['n_3-6']    = ppl['NUM OF CHILDREN UNDER 6']  - ppl['NUM OF CHILDREN UNDER 3']
demos['n_0-3']    = ppl['NUM OF CHILDREN UNDER 3']
demo_cols = list(demos.columns)
ppl[demo_cols] = demos

