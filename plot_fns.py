
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sb
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as cm

sttn_genre = pd.read_csv('data/StationGenreDescriptions.csv', index_col = 0)


#-------------------------------------------------------------------------------------------------#
### Cluster demo summary
def summarize_clusters(ppl, k, demo_cols):
    nk = ppl['cluster_'+str(k)].nunique()
    sum_k = pd.DataFrame([], index=['n_ppl']+demo_cols, columns=np.arange(nk))
    sum_k.loc['n_ppl'] = ppl['cluster_'+str(k)].value_counts()
    for i in range(nk):
        sum_k.loc[demo_cols, i] = ppl.loc[ppl['cluster_'+str(k)]==i, demo_cols].mean()
    return sum_k
#-------------------------------------------------------------------------------------------------#
### Condense: drop clusters with few members and reassign
def condense_clusters(df, Xr, model, k, min_cluster_size = 100):
    suf = '_'+str(k)
    n_members = df['cluster' + suf].value_counts()
    small_clusters = n_members.loc[n_members < min_cluster_size].index
    big_clusters   = n_members.loc[n_members >= min_cluster_size].index
    big_cluster_locs = np.array([model.cluster_centers_[i] for i in range(k) if n_members.iloc[i]>=min_cluster_size])
    new_cluster = df['cluster'+suf].copy()
    df['num_index'] = np.arange(0, df.shape[0])

    for p in df.loc[df['cluster'+suf].isin(small_clusters), 'num_index']:
        ### distance from reassigned points to retained cluster centers
        diffs = np.array([Xr[p] for i in range(big_cluster_locs.shape[0])]) - big_cluster_locs
        dists = [sum([x**2 for x in d]) for d in diffs]
        ### Reassign to closest big cluster
        new_cluster.loc[df.index[p]] = big_clusters[np.where(dists == min(dists))][0]
    return new_cluster

#-------------------------------------------------------------------------------------------------#
def getComparison(X, clustercol = 'cluster', agg='median'):
    k = len(X[clustercol].unique())
    X2 = X.drop(clustercol, axis=1)
    ### get the overall medians of each column to compare against
    if agg=='median':
        colMids = X2.median()
        colVars = X2.mad()
    elif agg=='mean':
        colMids = X2.mean()
        colVars = X2.std()
    # for c in X2.columns:
    #     if X2[c].dtype == 'bool':
    #         colVars[c] = np.sqrt(X2[c].mean()*(1-X2[c].mean())/X2.shape[0])
    ### get the medians of each parameter for each cluster
    ClusterProperties = pd.DataFrame(0, index = np.arange(k), columns = X2.columns)
    for i in ClusterProperties.index:
        if agg=='median':
            ClusterProperties.loc[i] = X2.loc[X[clustercol]==i].median()
        elif agg=='mean':
            ClusterProperties.loc[i] = X2.loc[X[clustercol] == i].mean()
    ### Compare the cluster medians to the overall medians, in terms of the MADs`
    Comparisons = (ClusterProperties - colMids)/colVars
    return Comparisons

#-------------------------------------------------------------------------------------------------#
### Plot the general/aggregate properties of each segment
### Function to plot how sales per category changes month by month, to be used below
def RelPlot(data, title='', xlab='default', ylab='default', RelMax='default', figsize=(15,7)):
    if RelMax == 'default':
        RelMax = min(data.abs().max().max(), 2.0)
    if xlab == 'default':
        xlab = data.columns
    if ylab == 'default':
        ylab = data.index
    plt.figure(figsize=figsize)
    with sb.axes_style("white"):
        im=plt.imshow(data,
                      interpolation='nearest', cmap=plt.get_cmap('bwr'),
                      vmin=-RelMax, vmax=RelMax);
    plt.colorbar(im,fraction=0.015, pad=0.04);
    plt.xticks([i+.25 for i in range(len(xlab))], xlab, fontsize=12, rotation=45, ha='right');
    plt.yticks([i     for i in range(len(ylab))], ylab, fontsize=15);
    plt.title(title, fontsize=20);

############################################################################################################
def plotClusters(ppl, X, X_per, Xr, clusters, k, vers, min_cluster_size,
                 genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols,
                 program_info):
    suf = '_'+str(k)
    pre = str(k)+'_'
    ### Cluster assignment for each point
    X['cluster' + suf] = clusters[k].labels_
    ### Trim smaller clusters
    X['cluster' + suf] = condense_clusters(X, Xr, clusters[k], k, min_cluster_size)
    n_clusters = X['cluster' + suf].nunique()
    # ### Let's see how many people are in each cluster
    ClustersBySize = X['cluster'+suf].value_counts()
    ### Renumber in order of size, to make the cluster names more meaningful.
    Relabel = {}
    for i, c in enumerate(ClustersBySize.index):
        Relabel[c] = i
    X['cluster'+suf] = [Relabel[c] for c in X['cluster'+suf]]
    ppl['cluster'+suf] = X['cluster'+suf]
    n_members = ppl['cluster' + suf].value_counts()

    ### Get plotting tools
    colors = cm.rainbow(np.linspace(0, 1, n_clusters))

    ### 2D plot
    f1, ax1 = plt.subplots(1, figsize=(8,8))
    for i in range(n_clusters):
        ax1.scatter(Xr[np.array(X['cluster'+suf]==i),0], Xr[np.array(X['cluster'+suf]==i),1],
                    color=colors[i], marker='o', label='k{0}, n={1}'.format(i, n_members.loc[i]));
    ax1.legend(loc='best');
    ax1.set_xlabel('Principal component 0', fontsize=20);
    ax1.set_ylabel('Principal component 1', fontsize=20);
    ax1.set_title('Clusters generated for k='+str(k), fontsize=20);
    f1.savefig('{0}/{1}cluster_2D.png'.format(vers, pre))
    plt.close(f1)

    # ### 3D plot
    f2 = plt.figure(figsize=[8, 8])
    ax2 = f2.add_subplot(111, projection='3d')
    for i in range(n_clusters):
        ax2.scatter(Xr[np.array(X['cluster'+suf]==i),0],
                    Xr[np.array(X['cluster'+suf]==i),1],
                    Xr[np.array(X['cluster'+suf]==i),2],
                    color=colors[i], alpha=0.25,  marker='o', label='k{0}, n={1}'.format(i, n_members.loc[i]));
    ax2.legend(loc='best');
    ax2.set_title('Clusters over first three PCs', fontsize=20);
    leg = ax2.legend()
    # for lh in leg.legendHandles:
    #     lh._legmarker.set_alpha(1)
    f2.savefig('{0}/{1}cluster_3D.png'.format(vers, pre))
    plt.close(f2)

    genre_col_codes = [l.split('_')[-1] for l in genre_cols]
    genre_col_descr = [sttn_genre.get_value(c, 'description') for c in genre_col_codes]
    genre_col_label = ['{0} ({1})'.format(genre_col_descr[i], genre_col_codes[i]) for i in range(len(genre_col_codes))]

    # ### Clustered columns in percentile space
    # X_per['cluster'+suf] = X['cluster'+suf]
    # clustercol_per_Comparisons = getComparison(X_per, agg='mean')
    # RelPlot(clustercol_per_Comparisons, 'Clustering on columns used in modeling')
    # plt.tight_layout()
    # plt.savefig('{0}/cluster_fitting{1}.png'.format(vers, suf))
    # plt.close('all')

    ### Plot top program cols
    ProgramComparisons = getComparison(ppl[list(pd.Series(program_info.index).dropna()) +
                                           ['cluster' + suf]], agg='mean', clustercol='cluster' + suf)
    n_shows = 40
    for page in [0,1,2,3,4]:
        start, end = page*n_shows, (page+1)*n_shows
        RelPlot(ProgramComparisons[program_info.index[start:end]],
                'Viewer Segments vs Programs Watched (rank {0}-{1})'.format(start, end),
                xlab=list(program_info.loc[program_info.index[start:end], 'program_name']))
        plt.tight_layout()
        plt.savefig('{0}/{1}cluster_programs_pg{2}.png'.format(vers, pre, page+1))
        plt.close('all')

    ### Plot tv columns
    TvComparisons = getComparison(ppl[tv_agg_cols+genre_cols+['cluster'+suf]], clustercol = 'cluster'+suf)
    RelPlot(TvComparisons, 'Viewer Segments vs TV columns',
            xlab = tv_agg_cols + genre_col_label)
    plt.tight_layout()
    plt.savefig('{0}/{1}cluster_tv.png'.format(vers, pre))
    plt.close('all')

    ### See whether tv behaviors predict purchasing behaviors
    BuyComparisons = getComparison(ppl[buy_cols + frac_buy_cols + ['hh_spent','cluster'+suf]], agg='mean', clustercol='cluster'+suf)
    RelPlot(BuyComparisons, 'Purchasing Habits', xlab = buy_cols + frac_buy_cols + ['hh_spent'])
    plt.tight_layout()
    plt.savefig('{0}/{1}cluster_purch.png'.format(vers, pre))
    plt.close('all')

    ### Viewer demographic info by cluster
    DemoComparisons = getComparison(ppl[demo_cols+['cluster'+suf]], agg='mean', clustercol='cluster'+suf)
    RelPlot(DemoComparisons, 'Viewer Cluster Demographics')
    plt.tight_layout()
    plt.savefig('{0}/{1}cluster_demos.png'.format(vers, pre))
    plt.close('all')

    print(n_members)
