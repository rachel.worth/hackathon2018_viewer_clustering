
### This script contains some functions and data structures used to read in the fixed-width data in read_tuning.py.
### Some of it is done manually and some is automatically generated from the format files.

import pandas as pd
import numpy as np
### Print wider in console
desired_width = 150
pd.set_option('display.width', desired_width)


def condense_fmt(df, pos_col = 'position'):
    '''Takes a format document as read into a dataframe and identfies the rows with position numbers,
    then, if the other fields were split across multiple lines, it condenses them into one. It also
    breaks the position number range into two numeric columns which can be given to a fixed-width
    file read statement.'''
    df['position_rows'] = df[pos_col].notnull()
    position_ind = df.loc[df.position_rows].index
    for i in range(len(position_ind)):
        if i < len(position_ind)-1:
            this_ind, next_ind = position_ind[i], position_ind[i+1]
        else:
            this_ind, next_ind = position_ind[i], df.index[-1]+1
        fragment = df.loc[this_ind:(next_ind-1)].copy()
        for c in fragment.columns[1:-1]:
            if any(fragment[c][1:].notnull()):
                fragment.loc[this_ind,c] = ' '.join(fragment[c].loc[fragment[c].notnull()])
        df.loc[this_ind] = fragment.loc[this_ind]
    df = df.loc[df.position_rows]
    num       = df[pos_col].str.match("[0-9]+",         as_indexer=True)
    num_range = df[pos_col].str.match("[0-9]+\-[0-9]+", as_indexer=True)
    df = df.loc[num]
    df[['range_start', 'range_end']] = df.position.str.split('-', expand=True)
    df.loc[df.range_end.isnull(), 'range_end'] = df.loc[df.range_end.isnull(), 'range_start']
    df[['range_start', 'range_end']] = df[['range_start', 'range_end']].astype('int')
    df['range_start'] = df.range_start - 1
    return df

########################################################################################################################
### B3B: Viewing records
########################################################################################################################
#### Manual names for this file, since there aren't too many columns and the fields don't have simple names in the file
# b3b_colnames = ['clct_day','hh','day','start_min','lds_set_number','good','acn_day','dma','tz','nti_sttn',
#             'day2','start_min2','drtn','end_min',
#             'plybk_lds_set_number','plybk_date','plybk_time','plybk_type',
#             'person','age','gender',
#             'genre_type','genre_alpha','genre_desc']
b3b_format = pd.read_fwf('data/format.b3b.txt', skipinitialspace = False,
            colspecs = [ [0, 15], [15, 80] ],
            header=None,
            names = ['position','description'])

b3b_format = condense_fmt(b3b_format)
b3b_format['title'] = b3b_format.description.str.replace(' - ', ' (').str.replace('/', ' (')\
                                .str.split('(', expand=True).iloc[:,0].str.strip()
### Manually add extra genre columns
b3b_format.loc[32] = [     '99', '', True,  98,  99, 'genre_code_type']
b3b_format.loc[33] = ['101-103', '', True, 100, 103, 'genre_code']
b3b_format.loc[34] = ['105-130', '', True, 104, 130, 'genre_descr']


########################################################################################################################
### TGB2: Household demographics
########################################################################################################################
b1010_format = pd.read_fwf('data/format.b10-10.txt', skipinitialspace = False,
            colspecs = [ [0, 10], [10, 19], [19, 38], [38, 100] ],
            header=None,
            names = ['position','format','title','description'])

b1010_format = condense_fmt(b1010_format)

########################################################################################################################
### TGB2: Household demographics
########################################################################################################################
tgb2_format = pd.read_fwf('data/format.tgb2.txt', skipinitialspace = False,
            colspecs = [ [0, 11], [11, 16], [16, 29], [29, 37], [37, 110] ],
            header=None,
            names = ['position','datatype','title','source','description'])

tgb2_format = condense_fmt(tgb2_format)

########################################################################################################################
### Combined person-level characteristics:
########################################################################################################################
b1010_format['file'] = 'b1010'
tgb2_format['file']  = 'tgb2'
char_format = pd.concat([
                b1010_format[['range_start', 'range_end', 'file', 'title', 'description']],
                 tgb2_format[['range_start', 'range_end', 'file', 'title', 'description']] ],
                axis=0, ignore_index=True)

char_format.loc[char_format.file == 'tgb2', ['range_start','range_end']] += \
    char_format.loc[char_format.file == 'b1010', 'range_end'].max() + 1

########################################################################################################################
