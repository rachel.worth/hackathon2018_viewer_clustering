import pandas as pd
pd.set_option('display.width', 180)
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as cm
import seaborn as sb
from sklearn.preprocessing import scale
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
import os
import string
########################################################################
def rms(x, axis=0):
    return (x**2).mean(axis=axis)**0.5
########################################################################################
def condense_clusters(col, nodes, df, min_cluster_size=100):
    new_cluster = col.copy()
    n_members = col.value_counts(ascending=True)
    for i, k in enumerate(n_members.index):
        if n_members.loc[k] < min_cluster_size:
            other_nodes = nodes.loc[n_members.iloc[(i + 1):].index, 'center']
            for p in new_cluster.loc[new_cluster == k].index:
                dists = [(sum((v - df.loc[p]) ** 2)) ** 0.5 for v in other_nodes]
                new_cluster.loc[p] = other_nodes.index[np.where(dists == min(dists))][0]
    return new_cluster

# ------------------------------------------------------------------------------------ #
### Rename in order of size
def rename_clusters(col):
    n_clusters = col.nunique()
    ClustersBySize = col.value_counts()
    ### Renumber in order of size, to make the cluster names more meaningful.
    Relabel = {}
    for i, c in enumerate(ClustersBySize.index):
        Relabel[c] = i
    return pd.Series([Relabel[c] for c in col], index = col.index), Relabel

# ------------------------------------------------------------------------------------ #
def plot3d(df_positions, col, plotcols=['pca0','pca1','pca2'], colnames=[],
           centroids=pd.DataFrame([]), knames=pd.DataFrame([])):
    assert len(plotcols)==3, 'ERROR: specify exactly three column names to plot'
    n_members = col.value_counts()
    if knames.shape[0] == 0:
        if centroids.shape[0] > 0:
            knames = pd.DataFrame(centroids.index, index=centroids.index, columns=['name'])
        else:
            knames = pd.DataFrame(n_members.index, index=n_members.index, columns=['name'])
    colors = cm.rainbow(np.linspace(0, 1, n_members.shape[0]))
    f2 = plt.figure(figsize=[8, 8])
    ax2 = f2.add_subplot(111, projection='3d')
    for i, n in enumerate(n_members.index):
        ax2.scatter(df_positions.loc[col == n, plotcols[0]],
                    df_positions.loc[col == n, plotcols[1]],
                    df_positions.loc[col == n, plotcols[2]],
                    color=colors[i], alpha=0.25, marker='o', label='{0}, n={1}'.format(n, n_members.loc[n]));
    for k in centroids.index:
        ax2.text(centroids.loc[k, plotcols[0]],
                 centroids.loc[k, plotcols[1]],
                 centroids.loc[k, plotcols[2]],
                 knames.loc[k, 'name'])
    if len(colnames) == 0:
        colnames = plotcols
    ax2.set_xlabel(colnames[0])
    ax2.set_ylabel(colnames[1])
    ax2.set_zlabel(colnames[2])
    leg = ax2.legend()
    for lh in leg.legendHandles:
        lh.set_alpha(1)
    # ax2.set_title('Clusters over first three PCs', fontsize=20);
    ax2.legend()
    return f2, ax2
# ------------------------------------------------------------------------------------ #
def plot3D_best(df, col, k1, k2, program_info):
    kdiffs = greatest_differences(df, col, k1, k2).index[:3]
    f2, ax2 = plot3d(df, col, plotcols=kdiffs,
           colnames=program_info.loc[[s.replace('frac_', '') for s in kdiffs], 'program_name'])
    return f2, ax2

# ------------------------------------------------------------------------------------ #
def run_pca(X, program_info, fit_cols, logEVR_thresh = -6, seed=None):
    if seed:
        pca = PCA()
    else:
        pca = PCA(random_state=seed)
    Xp = pd.DataFrame(pca.fit_transform(X), index=X.index, columns=['pca' + str(i) for i in range(X.shape[1])])
    pca_components = pd.DataFrame(pca.components_, columns=fit_cols).transpose()
    pca_components['names'] = list(program_info.loc[pca_components.index.str.replace('frac_', ''), 'program_name'])
    ### Reduce dimensionality
    EVR = pca.explained_variance_ratio_
    nvars = np.sum(np.log(EVR) > logEVR_thresh)
    Xr = Xp.iloc[:,:nvars]
    return Xp, Xr, nvars, pca
# ------------------------------------------------------------------------------------ #
def cluster_condense(Xr, k=18, n_runs = 10, min_cluster_size=100):
    ### Fit cluster
    runs = []
    for i in range(n_runs):
        clstr = KMeans(n_clusters=k, n_jobs=6)
        clstr.fit(Xr)
        runs.append(clstr)
    scores = [c.inertia_ for c in runs]
    cluster = runs[np.where([s == min(scores) for s in scores])[0][0]]
    c1 = pd.Series(cluster.labels_, Xr.index)
    ### df of cluster info
    nodes1 = pd.DataFrame(c1.value_counts().sort_index())
    nodes1.columns = ['n_ppl']
    nodes1['center'] = [[]] * nodes1.shape[0]
    for i in nodes1.index:
        nodes1.at[i, 'center'] = cluster.cluster_centers_[i]
    ### Condense clusters
    c2 = condense_clusters(c1, nodes1, Xr, min_cluster_size)
    ### Rename
    c3, relabel3 = rename_clusters(c2)
    return c3, cluster, relabel3
#--------------------------------------------------------------------------------------#
def pca_cluster_condense(X, program_info, fit_cols, k=18, logEVR_thresh = -6, n_runs = 10, min_cluster_size=100):
    Xp, Xr, nvars, pca = run_pca(X, program_info, fit_cols, logEVR_thresh)
    ### Fit cluster
    c3, cluster, relabel = cluster_condense(Xr, k=k, n_runs=n_runs, min_cluster_size=min_cluster_size)
    return c3, pca, nvars, cluster, relabel

#-------------------------------------------------------------------------------------------------#
def subdivide_cluster(ppl, X, col1, old_k, min_cluster_size=100):
    assert col1 not in ['colA', 'colB'], 'ERROR: colA and colB are reserved names in this function. Please rename your column'
    remaining = ppl.loc[ppl[col1] == old_k].copy()
    ### Fit clusters directly on X/X_per?
    remaining['colA'], c, relabel = cluster_condense(X.loc[ppl[col1] == old_k], min_cluster_size=min_cluster_size)
    n_members = remaining.colA.value_counts()
    assert n_members.shape[0] > 1, 'ERROR: no subdivisions found'
    ### Rename clusters whose names are used
    remaining['colB'] = remaining.colA
    for i in n_members.index:
        if (i != old_k) & (i in ppl[col1].unique()):
            increaseto = max(ppl[col1].max(), remaining.colB.max()) + 1
            print('reassigning {0} to {1}'.format(i, increaseto))
            remaining.loc[remaining.colB == i, 'colB'] = increaseto
            for k in relabel.keys():
                if relabel[k] == i:
                    relabel[k] = increaseto
    ### Merge with existing? reassign all points to nearest center
    ### Create new cluster column and output
    new_col = ppl[col1].copy()
    new_col.loc[new_col == old_k] = remaining['colB']
    return new_col, c, relabel

#-------------------------------------------------------------------------------------------------#
def getComparison(df, clustercol, agg='median'):
    ks = clustercol.value_counts().index
    # df = df.drop(clustercol, axis=1)
    df = df[[c for c in df.columns if df[c].nunique() > 1]]
    ### get the overall medians of each column to compare against
    if agg=='median':
        colMids = df.median()
        colVars = df.mad()
    elif agg=='mean':
        colMids = df.mean()
        colVars = df.std()
    ### get the medians of each parameter for each cluster
    ClusterProperties = pd.DataFrame([], index = ks, columns = df.columns)
    for i in ClusterProperties.index:
        if agg=='median':
            ClusterProperties.loc[i] = df.loc[clustercol == i].median()
        elif agg=='mean':
            ClusterProperties.loc[i] = df.loc[clustercol == i].mean()
    ### Compare the cluster medians to the overall medians, in terms of the MADs`
    Comparisons = (ClusterProperties - colMids)/colVars
    return Comparisons

#-------------------------------------------------------------------------------------------------#
### Cluster demo summary
def summarize_clusters(ppl, col, demo_cols, program_info, n_shows=5):
    ks = ppl[col].unique()
    sum_k = pd.DataFrame([], index=['n_ppl']+demo_cols, columns=ks)
    sum_k.loc['n_ppl'] = ppl[col].value_counts()
    for k in ks:
        sum_k.loc[demo_cols, k] = ppl.loc[ppl[col]==k, demo_cols].mean()
    ### add top shows for each
    ranked_shownames = list(program_info.loc[program_info.n_mins > 0].index)
    ProgramComparisons = getComparison(ppl[ ranked_shownames],
                                       agg='mean', clustercol=ppl[col])
    top_show_cols = ['show' + str(i) for i in range(1, n_shows + 1)]
    for c in top_show_cols:
        sum_k.loc[c] = np.nan
    for c in sum_k:
        sum_k.loc[top_show_cols, c] = list(program_info.loc[
                                        ProgramComparisons.loc[c].sort_values(ascending=False).dropna().iloc[:n_shows].index,
                                        'program_name'])
    return sum_k

#-------------------------------------------------------------------------------------------------#
### Plot the general/aggregate properties of each segment
### Function to plot how sales per category changes month by month, to be used below
def RelPlot(data, title='', xlab='default', ylab='default', RelMax='default', figsize=(15,7)):
    if RelMax == 'default':
        RelMax = min(data.abs().max().max(), 2.0)
    if xlab == 'default':
        xlab = data.columns
    if ylab == 'default':
        ylab = data.index
    plt.figure(figsize=figsize)
    with sb.axes_style("white"):
        im=plt.imshow(data.astype('float'),
                      interpolation='nearest', cmap=plt.get_cmap('bwr'),
                      vmin=-RelMax, vmax=RelMax);
    plt.colorbar(im,fraction=0.015, pad=0.04);
    plt.xticks([i+.25 for i in range(len(xlab))], xlab, fontsize=12, rotation=45, ha='right');
    plt.yticks([i     for i in range(len(ylab))], ylab, fontsize=15);
    plt.title(title, fontsize=20);

#-------------------------------------------------------------------------------------------------#
def membership_bars(col, fname):
    n_members = col.value_counts(ascending=True)
    f1, ax1 = plt.subplots(1)
    ax1.barh(range(n_members.shape[0]), n_members, align='center', alpha=0.5)
    ax1.set_yticks(range(n_members.shape[0]));
    ax1.set_yticklabels(n_members.index, ha='right', va='center')
    ax1.set_ylim(-1, n_members.shape[0])
    ax1.set_title('Cluster membership')
    ax1.set_xlabel('Number of people')
    f1.savefig(fname)
    plt.close(f1)
#-------------------------------------------------------------------------------------------------#
def plotClusters(ppl, col,
                 sttn_genre, genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols, program_info,
                 pre='', klist = []):

    genre_col_codes = [l.split('_')[-1] for l in genre_cols]
    genre_col_descr = [sttn_genre.get_value(c, 'description') for c in genre_col_codes]
    genre_col_label = {}
    for i in range(len(genre_cols)):
        genre_col_label[ genre_cols[i] ] = '{0} ({1})'.format(genre_col_descr[i], genre_col_codes[i])

    if len(klist) == 0:
        klist = list(col.value_counts().index)

    ### Bar chart of membership levels
    membership_bars(col, '{0}cluster_members.png'.format(pre))

    ### Plot top program cols
    ProgramComparisons = getComparison(ppl[pd.Series(program_info.index).dropna()],
                                       agg='mean', clustercol=col)
    n_shows = 40
    for page in [0,1,2,3,4]:
        start, end = page*n_shows, (page+1)*n_shows
        RelPlot(ProgramComparisons.loc[klist, program_info.index[start:end]],
                'Viewer Segments vs Programs Watched (rank {0}-{1})'.format(start, end),
                xlab=list(program_info.loc[program_info.index[start:end], 'program_name']), figsize=(18,10))
        plt.tight_layout()
        plt.savefig('{0}cluster_programs_pg{1}.png'.format(pre, page+1))
        plt.close('all')

    ### Plot tv columns
    TvComparisons = getComparison(ppl[tv_agg_cols+genre_cols], clustercol = col)
    ### drop columns with no variation
    TvComparisons = TvComparisons[TvComparisons.columns[TvComparisons.sum() != 0]]
    RelPlot(TvComparisons.loc[klist], 'Viewer Segments vs TV columns', figsize=(15,10),
            xlab = tv_agg_cols + [genre_col_label[g] for g in TvComparisons.columns if 'genre_' in g])
    plt.axvline(0.5, c='black')
    plt.axvline(5 - 0.5, c='black')
    plt.axvline(len(tv_agg_cols) - 0.5, c='black')
    plt.tight_layout()
    plt.savefig('{0}cluster_tv.png'.format(pre))
    plt.close('all')

    ### See whether tv behaviors predict purchasing behaviors
    BuyComparisons = getComparison(ppl[buy_cols + ['hh_spent'] + frac_buy_cols],
                                   agg='mean', clustercol=col)
    RelPlot(BuyComparisons.loc[klist], 'Purchasing Habits', figsize=(15,10))
    plt.axvline(len(buy_cols) - 0.5, c='black')
    plt.axvline(len(buy_cols) + 0.5, c='black')
    plt.tight_layout()
    plt.savefig('{0}cluster_purch.png'.format(pre))
    plt.close('all')

    ### Viewer demographic info by cluster
    DemoComparisons = getComparison(ppl[demo_cols], agg='mean', clustercol=col)
    RelPlot(DemoComparisons.loc[klist], 'Viewer Cluster Demographics', figsize=[18,10])
    plt.axvline( 6 - 0.5, c='black')#general
    plt.axvline(DemoComparisons.shape[1]-12.5, c='black')#race
    plt.axvline(DemoComparisons.shape[1]- 5.5, c='black')#hh size
    plt.tight_layout()
    plt.savefig('{0}cluster_demos.png'.format(pre))
    plt.close('all')

#-------------------------------------------------------------------------------------------------#
def greatest_differences(Xp, col, k1, k2):
    return (Xp.loc[col == k1].mean(axis=0) - Xp.loc[col == k2].mean(axis=0)).abs().sort_values(ascending=False)

#-------------------------------------------------------------------------------------------------#
### Get 20 top shows for a cluster
# shows = summarize_clusters(ppl, 'c1', demo_cols, program_info, n_shows=20)
# shows = shows.loc[[s for s in shows.index if 'show' in s]]

### creating a new cluster name file
# shows[11].drop_duplicates().to_csv('cluster_names/sports.csv')

### adding more shows to an existing cluster name file?
# name = 'sports'
# k = 11
# current  = shows[k]
# previous = pd.read_csv('cluster_names/{0}.csv'.format(name), index_col=0, header=None)[1]
# new = [s for s in current if not any(previous == s)]
# print(new)
# ### are you sure?
# ### are you???
# update   = pd.concat([current, previous], ignore_index=True).drop_duplicates()
# update.to_csv('cluster_names/{0}.csv'.format(name))

### Start tracking frequency???
# current = pd.DataFrame(shows1[k].copy())
# current.columns = ['name']
# current['counts'] = 1.
# previous = pd.read_csv('cluster_names/{0}.csv'.format(name), index_col=0, header=None)
# previous.columns = ['name']
# previous['counts'] = 1.
#
# mg = pd.merge(previous, current, left_on='name', right_on='name', how='outer').fillna(0)
# mg['counts'] = mg.counts_x + mg.counts_y
# mg['freq']   = mg.counts / mg.counts.max()
## mg[['name','counts','freq']].to_csv('cluster_names/{0}.csv'.format(name))

#-------------------------------------------------------------------------------------------------#
def name_clusters(shows, mainstream='none', dir='new_names'):
    ### number of shows provided
    n_shows = shows.shape[0]
    ### Read existing name files
    cluster_namefiles = [s for s in os.listdir(dir) if '.csv' in s]
    cluster_names = [s.replace('.csv','') for s in cluster_namefiles]
    cluster_key = {}
    for n in cluster_names:
        cluster_key[n] = pd.read_csv('{0}/{1}.csv'.format(dir, n), index_col=0, header=None)[1]
    ### Compare each cluster's shows to previous examples to find best name
    name_score = pd.DataFrame([], index = cluster_names, columns = shows.columns)
    for c in name_score.columns:
        for n in name_score.index:
            name_score.loc[n, c] = (shows[c].drop_duplicates().isin(cluster_key[n]).sum() /
                                                (min(cluster_key[n].shape[0], n_shows)+1) )
    ### Assign best match as name
    names = pd.DataFrame([], columns = ['name', 'score'])
    for k in name_score.columns:
        if name_score[k].max() >= 0.1:
            names.loc[k] = name_score[k].idxmax(), name_score[k].max()
        else:
            names.loc[k] = 'unknown', name_score[k].max()
    ### If no match with any existing cluster, default to mainstream
    names.loc[names.score==0,'name'] = 'mainstream'
    ### If there is still one dominant cluster, call it 'mainstream' regardless
    if mainstream != 'none':
        if names.loc[mainstream, 'score'] < 0.5:
            names.loc[mainstream] = ['mainstream', np.nan]
    ### Check for duplicates
    names['dummy'] = 1
    n_matches = names.groupby('name').dummy.count()
    if any(n_matches > 1):
        dupl = n_matches.loc[n_matches > 1]
        for n in dupl.index:
            for i, ind in enumerate(names.loc[names.name == n].sort_values('score', ascending=False).index):
                names.loc[ind, 'name'] += string.ascii_uppercase[i]
    ### Check for low scores
    if any(names.score <= 0.45):
        print('WARNING: some clusters poorly matched!')
        print(names.loc[names.score <= 0.45])
    ### Identify categories which could be improved? high match but also some not matched
    candidates = names.loc[(names.score >= 0.85) & (names.score < n_shows/(n_shows+1.))]
    if candidates.shape[0] > 0:
        print('Categories that could be learned from: {0}'.format(candidates.shape[0]))
        print(candidates)
    return names[['name','score']], name_score
#-------------------------------------------------------------------------------------------------#
### these results don't make a lot of sense, needs checking over
# def member_dist(X, col, k1, k2, centroids='none', cent1='none', cent2='none', doprint=True):
#     if cent1 == 'none':
#         cent1 = centroids.loc[k1][1:]
#     if cent2 == 'none':
#         cent2 = centroids.loc[k2][1:]
#     p = X.loc[col.isin([k1, k2])]
#     dists = pd.DataFrame({'d1': (p.subtract(cent1, axis=1)**2.).sum(axis=1)**0.5,
#                           'd2': (p.subtract(cent2, axis=1)**2.).sum(axis=1)**0.5})
#     if doprint:
#         print('People in {0} are median {1:0.2f} away from the center of {2}'.format(k1, dists.loc[col==k1, 'd2'].median(), k2))
#         print('            and median {0:0.2f} away from their own center'.format(dists.loc[col==k1, 'd1'].median()))
#         print('People in {0} are median {1:0.2f} away from the center of {2}'.format(k2, dists.loc[col==k2, 'd1'].median(), k1))
#         print('            and median {0:0.2f} away from their own center'.format(dists.loc[col==k2, 'd2'].median()))
#     return dists.loc[col==k1, 'd2'].median(), dists.loc[col==k2, 'd1'].median()
# ------------------------------------------------------------------------------------ #
def cluster_distances(X, centroids, col, blank_diagonal=True):
    groupX = X.iloc[:, 1:].copy()
    groupX['k'] = col
    dists = pd.DataFrame([])
    for k in centroids.index:
        dists[k] = (groupX.groupby('k').mean().subtract(centroids.loc[k], axis=1) ** 2).sum(axis=1) ** 0.5
    if blank_diagonal:
        for k in centroids.index:
            dists.loc[k,k] = np.nan
    return dists
# ------------------------------------------------------------------------------------ #
def ppl_distances(X, centroids, col, blank_current=False):
    X = X.iloc[:, 1:].copy()
    dists = pd.DataFrame([])
    for k in centroids.index:
        dists[k] = (X.subtract(centroids.loc[k], axis=1) ** 2).sum(axis=1) ** 0.5
    if blank_current:
        for p in X.index:
            dists.loc[p, col.loc[p]] = np.nan
    return dists
# ------------------------------------------------------------------------------------ #
def get_cluster_centers(X, c, relabel):
    centers = pd.DataFrame(c.cluster_centers_,
                            index=range(c.cluster_centers_.shape[0]),
                            columns=X.columns)
    centroids = pd.DataFrame([], columns = X.columns)
    for k in relabel.keys():
        centroids.loc[relabel[k]] = centers.loc[k]
    centroids = centroids.sort_index()
    return centroids
# ------------------------------------------------------------------------------------ #
### calculate dimensions for a grid of n subplots
def layout(n):
    n1 = int(np.ceil(n ** 0.5))
    if n1 * (n1 - 1) >= n:
        n2 = n1 - 1
    else:
        n2 = n1
    return n1, n2

# ------------------------------------------------------------------------------------ #
def plot_cluster_property(propcol, clustercol, bins, fname, names, ticknames=[]):
    f2, ax2 = plt.subplots( *layout(names.shape[0]), sharex=True)
    ax = ax2.flatten()
    for i, k in enumerate(names.index):
        ax[i].hist(propcol.loc[clustercol==k], bins = bins)
        ax[i].set_title('{0} {1}'.format(k, names.loc[k, 'base_name']))
        if len(ticknames) > 0:
            ax[i].set_xticks(bins[:(len(bins)-1)]+0.5)
            ax[i].set_xticklabels([ticknames[i] for i in bins[:(len(bins)-1)]], rotation=30, ha='right')
    ax[i].set_xlim(bins[0], bins[-1])
    plt.tight_layout()
    f2.subplots_adjust(wspace=0.1, hspace=.3)
    f2.savefig(fname)
    plt.close(f2)
# ------------------------------------------------------------------------------------ #
def cluster_show_coverage(ppl, kcol, program_info, fname, k=1, n_shows=20):
    '''For this cluster's top shows, how much do each of the members watch?'''
    ProgramComparisons = getComparison(ppl[ program_info.index],
                                       agg='mean', clustercol=kcol)
    top_shows = ProgramComparisons.loc[k].sort_values(ascending=False).iloc[:n_shows].index

    f1, ax1 = plt.subplots(*layout(n_shows), figsize=[25,15])
    f1.subplots_adjust(hspace=0.4, wspace=0.15, right=.98, bottom=.05, top=.95, left=0.05)
    ax2 = ax1.flatten()
    for i in range(n_shows):
        sb.distplot(ppl.loc[kcol == k, top_shows[i]], kde=False, ax=ax2[i],
                    bins=[0,5] + list(np.arange(30, ppl.loc[kcol == k, top_shows[i]].sort_values().iloc[-2], 30)))
        ax2[i].set_xticks(np.arange(0, ppl.loc[kcol == k, top_shows[i]].sort_values().iloc[-1], 60))
        ax2[i].set_title('{0}: {1}'.format(top_shows[i], program_info.loc[top_shows[i], 'program_name']))
        ax2[i].set_xlabel('')
    f1.text(0.5, 0.01, 'Minutes watched', ha='center', fontsize=14)
    f1.text(0.02, 0.5, 'Number of people', rotation=90, ha='center', fontsize=14)
    # plt.tight_layout()
    f1.savefig(fname)
    plt.close(f1)

# ------------------------------------------------------------------------------------ #
def cluster_person_coverage(ppl, kcol, program_info, fname, n_shows=20,
                            names=pd.DataFrame([], columns=['name'], dtype='str')):
    '''For each of this cluster's members, how many of the top shows do they watch?'''
    ProgramComparisons = getComparison(ppl[ program_info.index],
                                       agg='mean', clustercol=kcol)
    if names.shape[0] == 0:
        nk = kcol.nunique()
        for k in kcol.unique():
            names.loc[k, 'name'] = str(k) + ' - ' + str((kcol==k).sum())
    else:
        nk = names.shape[0]
    f1, ax1 = plt.subplots(*layout(nk), figsize=[15, 12])
    f1.subplots_adjust(hspace=0.4, wspace=0.15, right=.98, bottom=.05, top=.95, left=0.05)
    ax2 = ax1.flatten()
    for i, k in enumerate(names.index):
        top_shows = ProgramComparisons.loc[k].sort_values(ascending=False).iloc[:n_shows].index
        sb.distplot((ppl.loc[kcol==k, top_shows]>5).sum(axis=1), bins=np.arange(0, n_shows+1, 1), kde=False, ax=ax2[i])
        ax2[i].set_title(names.loc[k, 'name'])
        ax2[i].set_xlabel('')
    f1.text(0.5, 0.01, 'Number of shows', ha='center', fontsize=14)
    f1.text(0.02, 0.5, 'Number of people', rotation=90, ha='center', fontsize=14)
    f1.savefig(fname)
    plt.close(f1)


# ------------------------------------------------------------------------------------ #
# ------------------------------------------------------------------------------------ #
# ------------------------------------------------------------------------------------ #
# ------------------------------------------------------------------------------------ #
