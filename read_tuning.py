
### This script reads in viewer and tuning data according to the formatting set up in file_formatting.py.
import random
from file_formatting import *
import datetime
########################################################################################################################
### DMA info
dmas = pd.read_csv('../DMA_info.csv', index_col = 0)
dmas = dmas.loc[[s for s in dmas.index if '-' not in s]]
dmas.index = dmas.index.astype('int')

########################################################################################################################
### Person-level file
### Needs b1010, then tgb2 formatting
print('Read viewer data')
viewers = pd.read_fwf('data/b1010_tgb2.oct16.unified', skipinitialspace = False,
                      header=None, names = char_format['title'],
                      colspecs = list(char_format[['range_start','range_end']].itertuples(index=False)))
viewers['hh_prsn']  = viewers['Household Number'].astype('str') + '-' + viewers['Person Number'].astype('str')

########################################################################################################################
### Purchasing history data
print('Read purchasing data')

purchasing = pd.read_csv('data/purch_data.csv')
purchasing['month'] = [s[2:5] for s in purchasing.CAL_DT]

purch_by_dept = pd.pivot_table(data = purchasing, index = 'PANL_HHLD_ID_NBR', columns = 'DEPT_DESCR',
                               values='DOLLARS_SPENT', aggfunc = 'sum', fill_value = 0)
# purch_by_dept.to_csv('data/purch_hhs.txt', columns = [], header=False)

### Find avg hh spending by dma
purch_hh = purchasing.merge(viewers[['Household Number','NUM OF PERSONS','DMA CODE']],
                            left_on='PANL_HHLD_ID_NBR', right_on='Household Number', how='left')\
                     .groupby(['PANL_HHLD_ID_NBR','NUM OF PERSONS','DMA CODE'])['DOLLARS_SPENT'].sum().reset_index()
purch_hh['purch_per_prsn'] = purch_hh['DOLLARS_SPENT']/purch_hh['NUM OF PERSONS']

purch_hh_dma = pd.DataFrame(purch_hh.groupby('DMA CODE').purch_per_prsn.mean().sort_values(ascending=False))
purch_hh_dma.columns = ['purch_per_prsn']
purch_hh_dma['n_viewers'] = viewers.groupby('DMA CODE')['hh_prsn'].count()
purch_hh_dma = purch_hh_dma.join(dmas[['name','state']], how='left')

purch_hh_state = purch_hh_dma.groupby('state').purch_per_prsn.median().sort_values()


########################################################################################################################
### Population to use -- run commented portion to generate new population list
# vers = 'Northeast'
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
### All of certain states
# if vers == 'Mideast':
#     use_dmas = purch_hh_dma.loc[dmas.state.isin(['North Carolina','Virginia','Maryland'])].index
# elif vers == 'Northeast':
#     # 3146 hhs, 8114 ppl, 4656231 tuning events
#     use_dmas = purch_hh_dma.loc[dmas.state.isin(['Connecticut','Massachusetts','Pennsylvania','New York','Rhode Island'])].index
#
# hh_list = list(viewers.loc[viewers['DMA CODE'].isin(use_dmas), 'Household Number']\
#                                .astype('str').str.zfill(7).unique())
#
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
### Equal metro/nonmetro
# all_metro_hhs    = list(viewers.loc[(viewers['Household Number'].isin(purch_by_dept.index)) &
#                                     (viewers['METRO CNTY IND']=='Y'), 'Household Number']\
#                                .astype('str').str.zfill(7).unique())
# all_nonmetro_hhs = list(viewers.loc[(viewers['Household Number'].isin(purch_by_dept.index)) &
#                                     (viewers['METRO CNTY IND']=='N'), 'Household Number']\
#                                .astype('str').str.zfill(7).unique())
# ## (9418, 1958)
# n_hhs = min(len(all_metro_hhs), len(all_nonmetro_hhs))
# print('Pulling {0} HHs'.format(n_hhs))
#
# metro_hhs    = random.sample(all_metro_hhs,    int(n_hhs/2))
# nonmetro_hhs = random.sample(all_nonmetro_hhs, int(n_hhs/2))
# hh_list = metro_hhs + nonmetro_hhs

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
### Write hh list to file
# with open('{0}/hh_list.txt'.format(vers), 'w') as f:
#     f.write('\n'.join(hh_list))
#     f.write('\n')
### To pull this tuning only, run this script (not sure how to do from python yet)
# import subprocess
# subprocess.check_output('cut_by_hh_prsn_id.bash', shell=True)

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
### Load already-created hh list
# hh_list = pd.read_csv('{0}/hh_list.txt'.format(vers), header=None, names = ['Household Number'])

#----------------------------------------------------------------------------------------------------------------------#
# stations = pd.read_fwf('data/tgb3.feb17.unified', header=None, index_col=False, skipinitialspace = False,
#                        colspecs = [ (0,11),     (11,46),  (46,50),    (50,20000)],
#                        names   = ['hh_id', 'date_code','n_sttns','sttn_id_str'])
# stations['sttn_id_list'] = stations.sttn_id_str.str.split(' ')
### There are some stations without spaces in between for some reason, which don't get split
### Still need to fix that...
# for i in range(stations.shape[0]):
#     stations.at[i, 'sttn_id_list'] =

#----------------------------------------------------------------------------------------------------------------------#
### Aggs from tuning data on databricks, incl. gracenote
print('read tuning_aggs')
tuning_aggs = pd.read_csv('data/viewer_info.csv')
tuning_aggs['hh_prsn'] = tuning_aggs['hh_id'].astype('str') + '-' + tuning_aggs['prsn_id'].astype('str')
tuning_aggs = tuning_aggs.set_index('hh_prsn')
daypart_cols = ['night','morning','afternoon','evening']
weekday_cols = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']
genre_cols = [c for c in tuning_aggs.columns if c[:6] == 'genre_']
show_cols  = [c for c in tuning_aggs.columns if c[:2] == 'SH']
movie_cols = [c for c in tuning_aggs.columns if c[:2] == 'MV']
sp_cols    = [c for c in tuning_aggs.columns if c[:2] == 'SP']
program_cols = show_cols + movie_cols + sp_cols

daypart_frac_cols = []
weekday_frac_cols = []
genre_frac_cols   = []
program_frac_cols = []
for cols in [[daypart_cols, daypart_frac_cols],
             [weekday_cols, weekday_frac_cols],
             [  genre_cols,   genre_frac_cols],
             [program_cols, program_frac_cols]]:
    for c in cols[0]:
        if 'frac_'+c not in tuning_aggs.columns:
            tuning_aggs['frac_'+c] = tuning_aggs[c]/tuning_aggs.n_mins
        if c in cols[0] and 'frac_'+c not in cols[1]:
            cols[1].append('frac_'+c)

### Compare viewer list between data sources
tuning_hhs = tuning_aggs.hh_id.unique()
vwr_hhs    = viewers['Household Number'].unique()
purch_hhs  = purch_hh.PANL_HHLD_ID_NBR.unique()

### 17165, 17521, 11376 hhs (when was this?)
### 17165 17521 6957 - 7/6/17 (not sure why the difference! -> 43,375 in ppl)
print('n_hhs:', len(tuning_hhs), len(vwr_hhs), len(purch_hhs))
### 17165 common hhs in tuning and viewer data (i.e. all from tuning)
print(len(list(set(tuning_hhs).intersection(set(vwr_hhs)))))
### 6850 hhs in common btwn tuning and purchasing data
print(len(list(set(tuning_hhs).intersection(set(vwr_hhs)).intersection(set(purch_hhs)))))

#----------------------------------------------------------------------------------------------------------------------#
### Read in the four weeks of tuning data and combine into one df
# print('Read tuning data')
# tuning_cols = ['Household Number','Person Number','Good',
#                '12A-12A day','Start Minute','Duration','End Minute','NTI Station code',
#                'genre_code','genre_descr']
# tuning = pd.read_fwf('{0}/b3b.fwf'.format(vers), skipinitialspace = False,
#                      header=None, names=b3b_format['title'],
#                      colspecs=list(b3b_format[['range_start', 'range_end']].itertuples(index=False)),
#                      usecols = tuning_cols)
# tuning = tuning.loc[tuning.Good == 'G']
# tuning['hh_prsn'] = tuning['Household Number'].astype('str') + '-' + tuning['Person Number'].astype('str')
# tuning = tuning[['hh_prsn'] + tuning_cols]
#
# ### Sttn code conversion
# nti_conv = pd.read_fwf('data/nti.nsi.stns.feb17',
#                        header=None, names=['nti_sttn','nsi_sttn','call','descr'],
#                        colspecs=[(0,10), (12,22), (28,33), (35,81)])
# tuning_nsi = tuning.merge(nti_conv[['nti_sttn','nsi_sttn']],
#                       left_on='NTI Station code', right_on='nti_sttn', how='inner')
#
# ### .drop_duplicates() if necessary?
#
# ########################################################################################################################
#
# ### Read program info
# program = pd.read_csv('data/gracenote_program_info.csv', index_col = 0, encoding = 'utf-8')
#
# tn_sttns = tuning_nsi.nsi_sttn.unique()
# pr_sttns = program.NLSN_STATION_ID.unique()
#
# both    = [x for x in tn_sttns if x     in pr_sttns]
# tn_only = [x for x in tn_sttns if x not in pr_sttns]
#
# ### This gives memory error
# # tn_prg = tuning_nsi.merge(program, left_on = 'nsi_sttn', right_on ='NLSN_STATION_ID', how='left')
# # tn_prg = tn_prg.loc[(tn_prg.ssn_sttm_utc <= tn_prg.prg_sttm_utc) &
# #                     (tn_prg.ssn_edtm_utc >= tn_prg.prg_edtm_utc)]
#
# # ------------------
# # program = pd.read_csv('data/program_feb17.csv')
# # program['start_time'] = pd.to_datetime(program.start_time)
# # program['end_time'] = pd.to_datetime(program.end_time)
# #
# # sttns_tuning  = tuning_nsi['nsi_sttn'].unique()
# # sttns_program = program['STATION_CODE'].unique()
# # overlap_sttns = list(set(sttns_tuning).intersection(set(sttns_program)))
# # print(len(sttns_tuning), len(sttns_program), len(overlap_sttns))
# #
# # program['drtn'] = (program.end_time - program.start_time).astype('timedelta64[s]')/60
# # program.loc[program.drtn < 0, 'end_time'] += datetime.timedelta(days=1)
# # program['drtn'] = (program.end_time - program.start_time).astype('timedelta64[s]')/60
# #
# # cut_prg = program.loc[ program.STATION_CODE.isin(overlap_sttns),
# #           ['STATION_CODE','start_time','end_time','NATIONAL_SERVICE_TIME_ZONE_ID',
# #            'DISTRIBUTOR_NAME','ORIGINATOR_CALL_LETTERS',
# #            'PROGRAM_NAME','PROGRAM_SUMMARY_TYPE_CODE','PROGRAM_SUMMARY_TYPE_DESC']]
# #
# # exp_prg = cut_prg.copy()
# # exp_prg['start_mins'] = [[]]*exp_prg.shape[0]
# # for i in range(exp_prg.shape[0]):
# #     this_row = exp_prg.iloc[i:(i+1)]
# #     trange = list(pd.date_range(this_row.start_time.iloc[0], this_row.end_time.iloc[0], freq='min'))
# #     exp_prg.at[exp_prg.index[i], 'start_mins'] = trange
# #     if i % 1000 == 0:
# #         print('{0}/{1}'.format(i, cut_prg.shape[0]))
# #
# # ### Expand at QH level
# # qh_bins = 15*np.arange(97)
# #
# # cut_prg['qh'] = np.nan
# # qh_prg = cut_prg.iloc[0:0,:]
# # for i in range(96):
# #     cut_prg.loc[(cut_prg.)]
# #
# #
# #
# # #################################
# # cut_prg['min'] = np.nan
# # exp_prg = cut_prg.iloc[0:0,:]
# # for i in range(cut_prg.shape[0]):
# #     this_row = cut_prg.iloc[i:(i+1)]
# #     trange = pd.date_range(this_row.start_time.iloc[0], this_row.end_time.iloc[0], freq='min')
# #     new_bit = pd.concat([this_row]*len(trange), ignore_index=True, axis=0)
# #     new_bit['min'] = trange
# #     exp_prg = pd.concat([exp_prg, new_bit], ignore_index=True)
# #     if i%100 == 0:
# #         print('{0}/{1}, {2}'.format(i, cut_prg.shape[0], exp_prg.shape[0]))
# # exp_prg.to_csv('minexpl_program.csv')
# #
# # #######
# # tuning['date'] = datetime.datetime(2017, 1, 1) + \
# # pd.to_timedelta(pd.Series([int(s[2:])-1 for s in tuning['12A-12A day'].astype('str').str.zfill(5)], index=tuning.index), unit='D')
# # tuning['start_min'] = tuning.date + pd.to_timedelta(tuning['Start Minute'], unit='m')
# # tuning['end_min']   = tuning.date + pd.to_timedelta(tuning['End Minute'], unit='m')
# # tuning['drtn']      = (tuning.end_min - tuning.start_min).astype('timedelta64[s]')/60
# # tuning.loc[(tuning.drtn < 0) &
# #            (tuning['End Minute'] == 0), 'end_min'] += datetime.timedelta(days=1)
# # tuning['drtn']      = (tuning.end_min - tuning.start_min).astype('timedelta64[s]')/60
# #
# # # tuning[['hh_prsn','12A-12A day','Start Minute','nsi_sttn','tz_id']].merge(exp_prg,
# # #              left_on  = ['mi_viwed_sttm_lcl','sttn','tz?'],
# # #              right_on = ['min','nsi_sttn','tz_id'],
# # #              how='inner')
# # #
# #
