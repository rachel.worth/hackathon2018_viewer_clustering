import pandas as pd
pd.set_option('display.width', 180)
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as cm
import seaborn as sb
from sklearn.preprocessing import scale
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
# from plot_fns import *

############################################################################################
print('Reading data')
from prep_for_clustering import *
from functions import *

vers = vers + '_frac'
############################################################################################
### Fit clusters on the following columns
nShows = 500
fit_cols = ['frac_'+c for c in program_info.index[:nShows]] #program_cols
# fit_cols = ['genre_'+str(n) for n in ['999','919','417']] + [c for c in program_info.index[:nShows]] #program_cols
X = pd.DataFrame(scale(ppl[fit_cols]), columns = fit_cols, index = ppl.index)
### Transform data to percentiles
X_per = X.rank(pct=True, axis=0) - 0.5

############################################################################################
### Get pca coordinates for plotting only -- not used in fitting
Xp, Xr, nvars, pca = run_pca(X, program_info, fit_cols)

### First pass
print('First clustering')
ppl['c1'], c1, relabel1 = cluster_condense(X)
### Get center coordinates from clustering objects
# centroids_k1 = get_cluster_centers(X, c1, relabel1)
# ### Summarize cluster properties
# sum1 = summarize_clusters(ppl, 'c1', demo_cols, program_info, n_shows=20)
# shows1 = sum1.loc[[i for i in sum1.index if 'show' in i]]
# c1_names, name_score1 = name_clusters(shows1)
# ### 3d plot
# plotcols = [greatest_differences(X, ppl.c1, 0, i).index[0] for i in range(1,4)]
# plot3d(X, ppl.c1, plotcols=plotcols, centroids=centroids_k1, knames=c1_names)

############################################################################################
############################################################################################
### Iteratively attempt to further break down the big central cluster and spin off new ones
phase1 = 10
clusters = {1: c1}
relabels = {1: relabel1}
print(ppl.c1.value_counts())
print('Phase 1: iteratively finding new clusters ({0} passes)'.format(phase1))
for i in range(2, phase1+1):
    try:
        ppl['c'+str(i)], clusters[i], relabels[i] = subdivide_cluster(ppl, X, 'c'+str(i-1), 0)
        print(ppl['c'+str(i)].value_counts())
    except:
        ppl['c' + str(i)] = ppl['c' + str(i-1)]
        print('{0}: no change, {1} clusters'.format(i, ppl['c' + str(i)].nunique()))

############################################################################################
############################################################################################
### Now try a few times on the second largest cluster?
f = phase1
ppl.groupby(['c'+str(i) for i in range(1,phase1)]).age.count()

### Summarize cluster properties
sums = summarize_clusters(ppl, 'c'+str(f), demo_cols, program_info, n_shows=20)
shows = sums.loc[[i for i in sums.index if 'show' in i]]
c_names, name_score = name_clusters(shows)

### Attempt to subdivide groups > 1000
phase2 = 2
# nonhut_k = c_names.loc[c_names.name.str.contains('nonHUT')].index
n_members = ppl['c'+str(f)].value_counts()
large_k = n_members.loc[n_members>1000].index

print('Phase 2: iteratively finding new clusters ({0}x{1} passes)'.format(phase2, len(large_k)))
for k in large_k:
    print('dividing #{0}'.format(k))
    for iter in range(1, phase2+1):
        cluster_col_names = ppl.columns[ppl.columns.str.match('c[0-9]+')]
        max_c_num = cluster_col_names.str.replace('c', '').astype('int').max()
        i = max_c_num + 1
        print('try c'+str(i))
        try:
            ppl['c' + str(i)], clusters[i], relabels[i] = subdivide_cluster(ppl, X, 'c' + str(i - 1), k, min_cluster_size=50)
            print(ppl['c' + str(i)].value_counts())
        except:
            print('{0}: no change'.format(i))
            ppl['c' + str(i)] = ppl['c' + str(i - 1)]

############################################################################################
############################################################################################
### Analyze the clusters
cluster_col_names = ppl.columns[ppl.columns.str.match('c[0-9]+')]
max_c_num = cluster_col_names.str.replace('c', '').astype('int').max()

f = max_c_num
ppl.groupby(['c'+str(i) for i in range(1,f+1)]).age.count()

### last column name
ck = 'c'+str(f)

n_members = ppl[ck].value_counts()


### Summarize cluster properties
print('Naming final clusters')
sums = summarize_clusters(ppl, ck, demo_cols, program_info, n_shows=20)
shows = sums.loc[[i for i in sums.index if 'show' in i]]
c_names, name_score = name_clusters(shows)
c_names['base_name'] = c_names.name.copy()
c_names['name'] = c_names.base_name + ' - ' + c_names.index.astype('str').str.pad(2) + ' - ' + n_members.astype('str').str.pad(4)
c_names = c_names.sort_values('base_name')

### Name column
cn = ck+'_names'
ppl[cn] = [c_names.loc[k, 'name'] for k in ppl[ck]]
### Number of clusters
nk = c_names.shape[0]

############################################################################################
########################## THIS WILL OVERWRITE #############################################
############################################################################################
### Save to csv
print('Cluster generation complete; saving to file')
ppl.to_csv(vers+'/{0}_ppl.csv'.format(nk))

### Make heatmaps
print('Creating heatmaps')
plotClusters(ppl, ppl[cn],
             sttn_genre, genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols, program_info,
             pre=vers+'/{0}_'.format(nk), klist = c_names.name)

### Plot show coverage per cluster?
cluster_person_coverage(ppl, ppl[ck], program_info, fname=vers + '/{0}_pplhists.png'.format(nk), names=c_names)
for k in [6,8,11,5,9]:
    cluster_show_coverage(ppl, ppl[ck], program_info, k=k,
                          fname=vers + '/{0}_showhists_k{1}_{2}.png'.format(nk, k, c_names.loc[k, 'base_name']))

### Age distribution per cluster
# income_bins = list(np.arange(0, 201, 25))+[501]
print('Creating histograms')
edLevel = pd.cut(ppl['HOH EDUCATION CODE'], bins=[0,8,12,12.1,16,16.1,18.1,21], right=False,
                 labels=range(0,7))
edlevelnames = {0: 'no HS',
                1: 'some HS',
                2: 'HS',
                3: 'some college',
                4: '4yr deg',
                5: 'masters',
                6: 'PhD'}

pname = vers+'/{0}_{1}_distrn.png'
plot_cluster_property(edLevel,                   ppl[ck], np.arange(0,  8), pname.format(nk,'edu'), c_names, edlevelnames)
plot_cluster_property(ppl.age,                   ppl[ck], np.arange(0, 106,  5), pname.format(nk,'age'),    c_names)
plot_cluster_property(ppl['INCOME AMT'],         ppl[ck], np.arange(0, 501, 25), pname.format(nk,'income'), c_names)

print('Finding centroids in various coordinates')
### Find real-coordinate centers for each cluster
centroids_real = pd.DataFrame(ppl[ck].value_counts())
centroids_real.columns = ['n_ppl']
for c in fit_cols:
    centroids_real[c] = np.nan
for k in centroids_real.index:
    centroids_real.at[k, fit_cols] = ppl.loc[ppl[ck]==k, fit_cols].mean()
### Centroids in X coords (ppl, standardized)
centroids_X = pd.DataFrame(ppl[ck].value_counts())
centroids_X.columns = ['n_ppl']
for c in X.columns:
    centroids_X[c] = np.nan
for k in centroids_X.index:
    centroids_X.at[k, X.columns] = X.loc[ppl[ck] == k, X.columns].mean()
### Centroids in pca coords
centroids_p = pd.DataFrame(ppl[ck].value_counts())
centroids_p.columns = ['n_ppl']
for c in Xp.columns:
    centroids_p[c] = np.nan
for k in centroids_p.index:
    centroids_p.at[k, Xp.columns] = Xp.loc[ppl[ck] == k, Xp.columns].mean()

### Get new centers direct from clustering model
centroids_k = {}
for i in range(1,f+1):
    if i in clusters.keys():
        centroids_k[i] = get_cluster_centers(X, clusters[i], relabels[i])
# centroids_k = pd.concat([centroids_k[i] for i in range(f) if i in centroids_k.keys()])

# ---------------------------------------------------------------------------- #
print('3D plots')
### Visualize in real coordinates
plotcols = [greatest_differences(X, ppl[ck], 0, i).index[0] for i in [1, 2, 3]]
f1, ax1 = plot3d(X, ppl[ck], plotcols=plotcols, centroids=centroids_X, knames=c_names,
       colnames=program_info.loc[[c.replace('frac_','') for c in plotcols], 'program_name'])
f1.savefig(vers+'/{0}_3D_X.png'.format(nk))

### 3d plot in PCA coords
kdiffs = ['pca0','pca1','pca2']
# kdiffs = [greatest_differences(Xp, ppl[ck], 0, k).index[0] for k in [??]]
f2, ax2 = plot3d(Xp,
                 ppl[ck], plotcols=kdiffs,
                 centroids=centroids_p,
                 knames   =    c_names)
f2.savefig(vers+'/{0}_3D_pca.png'.format(nk))

############################################################################################
### Reassign cluster members?
# pdist = ppl_distances(X, centroids_k, ppl[ck])
# check_clusters = ppl[[ck]].copy()
# check_clusters['new'] = pdist.idxmin(axis=1)
# check_clusters['dummy'] = 1
# ### newly assigned clusters not quite the same as the original, in unexpected ways
# # ppl[ck+'_new'] = check_clusters.new

############################################################################################
############################# OLD VERSION ##################################################
############################################################################################
# ### Round 2
# ppl['c2'], c2, relabel2 = subdivide_cluster(ppl, X, 'c1', 0)
# ppl.groupby(['c1','c2']).soda.count()
# ### Get new centers
# centroids_k2 = get_cluster_centers(X, c2, relabel2)
# centroids_k = pd.concat([centroids_k1.loc[1:], centroids_k2])
# ### Summarize cluster properties
# sum2 = summarize_clusters(ppl, 'c2', demo_cols, program_info, n_shows=20)
# shows2 = sum2.loc[[i for i in sum2.index if 'show' in i]]
# c2_names, name_score2 = name_clusters(shows2, mainstream=0)
# ### Visualize
# plotcols = [greatest_differences(X, ppl.c2, 0, i).index[0] for i in [11,12,13]]
# plot3d(X, ppl.c2, plotcols=plotcols, centroids=centroids_k, knames=c2_names)
#
# ### Summarize cluster properties
# # sum2 = summarize_clusters(ppl, 'c2', demo_cols, program_info, n_shows=20)
#
# ### Assign cluster names to each person
# ppl['c2_names'] = [c2_names.loc[k, 'name'] for k in ppl.c2]
#
# ### Make heatmaps
# plotClusters(ppl, ppl['c2_names'],
#              sttn_genre, genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols, program_info,
#              pre=vers+'/c4_')
#
# ### Find real-coordinate centers for each cluster
# centroids_real = pd.DataFrame(ppl.c2.value_counts())
# centroids_real.columns = ['n_ppl']
# for c in fit_cols:
#     centroids_real[c] = np.nan
# for k in centroids_real.index:
#     centroids_real.at[k, fit_cols] = ppl.loc[ppl['c2']==k, fit_cols].mean()
# ### Centroids in X coords (ppl, standardized)
# centroids_X = pd.DataFrame(ppl.c2.value_counts())
# centroids_X.columns = ['n_ppl']
# for c in X.columns:
#     centroids_X[c] = np.nan
# for k in centroids_X.index:
#     centroids_X.at[k, X.columns] = X.loc[ppl['c2'] == k, X.columns].mean()
# ### Centroids in pca coords
# centroids_p1 = pd.DataFrame(ppl.c2.value_counts())
# centroids_p1.columns = ['n_ppl']
# for c in Xp.columns:
#     centroids_p1[c] = np.nan
# for k in centroids_p1.index:
#     centroids_p1.at[k, Xp.columns] = Xp.loc[ppl['c2'] == k, Xp.columns].mean()
#
# ### 3d plot
# news = list(c2_names.loc[c2_names.name.str.contains('news')].index)
# kids = list(c2_names.loc[c2_names.name.str.contains('kids')].index)
# # kdiffs = ['pca0','pca1','pca2']
# kdiffs = [greatest_differences(Xp, ppl.c2, 0, k).index[0] for k in [5,8,11]]
# f1, ax1 = plot3d(Xp.loc[ppl.c2.isin(kids)],
#                  ppl.loc[ppl.c2.isin(kids), 'c2'], plotcols=kdiffs,
#                  centroids=centroids_p1,
#                  knames   =    c2_names)
# # ax1.set_xlim(-1, 1)
# # ax1.set_ylim(-1, 1)
# # ax1.set_zlim(-1, 1)
#
# ### Age distribution per cluster
# plot_cluster_property(ppl.age,                   ppl.c2, np.arange(0, 106,  5), vers+'/c4_age_distrn.png',    c2_names)
# plot_cluster_property(ppl['INCOME AMT'],         ppl.c2, np.arange(0, 501, 25), vers+'/c4_income_distrn.png', c2_names)
# plot_cluster_property(ppl['HOH EDUCATION CODE'], ppl.c2, np.arange(0,  21,  2), vers+'/c4_ed_distrn.png',     c2_names)
#
# ### Reassign clusters?
# pdist = ppl_distances(X, centroids_k, ppl.c1)
# check_clusters = ppl[['c2']].copy()
# check_clusters['new'] = pdist.idxmin(axis=1)
# check_clusters['dummy'] = 1
# ### newly assigned clusters not quite the same as the original, in unexpected ways
# ppl['c2_new'] = check_clusters.new
# # kdiffs = [greatest_differences(Xp, ppl.c2_new, 0, k).index[0] for k in [10,11,1]]
# kdiffs = ['pca8','pca23','pca9']
# f1, ax1 = plot3d(Xp.loc[ppl.c2_new.isin([0,10])], ppl.loc[ppl.c2_new.isin([0,10]),'c2_new'], plotcols=kdiffs,
#                  centroids=centroids_p1,
#                  knames   =    c2_names)
#
# ### nonHUT is really more just low-DVR??
# f1, ax1 = plot3d(X.loc[ppl.c2_new.isin([0,10])], ppl.loc[ppl.c2_new.isin([0,10]),'c2_new'],
#                  plotcols=X.columns[:3],
#                  colnames=program_info.loc[[s.replace('frac_','') for s in X.columns[:3]], 'program_name'],
#                  centroids=centroids_X,
#                  knames   =    c2_names)
#
# ########################################################################################
# ### Third pass?
# ppl['c3'], c3, relabel3 = subdivide_cluster(ppl, X, 'c2_new', 0)
# ### Summarize cluster properties
# sum3 = summarize_clusters(ppl, 'c3', demo_cols, program_info, n_shows=20)
# shows3 = sum3.loc[[i for i in sum3.index if 'show' in i]]
# c3_names, name_score3 = name_clusters(shows3, mainstream=0)
# ### Get new centers
# # centroids_k3 = get_cluster_centers(X, c3, relabel3)
# # centroids_k = centroids_k = pd.concat([centroids_k1.loc[1:], centroids_k2.loc[1:], centroids_k3])
# centroids_p3 = pd.DataFrame(ppl.c3.value_counts())
# centroids_p3.columns = ['n_ppl']
# for c in Xp.columns:
#     centroids_p3[c] = np.nan
# for k in centroids_p3.index:
#     centroids_p3.at[k, Xp.columns] = Xp.loc[ppl['c3'] == k, Xp.columns].mean()
#
# # centroids_real
# # centroids_X
# ### Visualize
# plotcols = [greatest_differences(Xp, ppl.c3, 0, i).index[0] for i in [14,15,16]]
# # plotcols = ['pca0','pca1','pca2']
# # plotcols = greatest_differences(Xp, ppl.c3, 14, 15).index[0:3]
# plot3d(Xp, ppl.c3, plotcols=plotcols,
#        centroids=centroids_p3, knames=c3_names)
# ppl['c3_names'] = [c3_names.loc[i,'name'] for i in ppl.c3]
# plotClusters(ppl, ppl['c3_names'],
#              sttn_genre, genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols, program_info,
#              pre=vers+'/c4_')
#
#
# ########################################################################################
# ### Fourth pass
# ppl['c4'], c4, relabel4 = subdivide_cluster(ppl, X, 'c3', 0)
# ### Summarize cluster properties
# sum4 = summarize_clusters(ppl, 'c4', demo_cols, program_info, n_shows=20)
# shows4 = sum4.loc[[i for i in sum4.index if 'show' in i]]
# c4_names, name_score4 = name_clusters(shows4, mainstream=0)
#
#
# clusters = {1: c1, 2:c2, 3:c3, 4:c4}
# relabels = {1: relabel1, 2:relabel2, 3:relabel3, 4:relabel4}
# for i in [5,6,7]:
#     print(i)
#     try:
#         ppl['c'+str(i)], clusters[i], relabels[i] = subdivide_cluster(ppl, X, 'c'+str(i-1), 0)
#     except:
#         print('{0}: no change'.format(i))
#         ppl['c' + str(i)] = ppl['c' + str(i-1)]
#
# ### Summarize cluster properties
# sum7 = summarize_clusters(ppl, 'c7', demo_cols, program_info, n_shows=20)
# shows7 = sum7.loc[[i for i in sum7.index if 'show' in i]]
# c7_names, name_score7 = name_clusters(shows7, mainstream=0)
#
# ppl['c7_names'] = [c7_names.loc[i, 'name'] for i in ppl.c7]
# plotClusters(ppl, ppl['c7_names'],
#              sttn_genre, genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols, program_info,
#              pre=vers+'/c7_')
#
# centroids_p7 = pd.DataFrame(ppl.c7.value_counts())
# centroids_p7.columns = ['n_ppl']
# for c in Xp.columns:
#     centroids_p7[c] = np.nan
# for k in centroids_p7.index:
#     centroids_p7.at[k, Xp.columns] = Xp.loc[ppl['c7'] == k, Xp.columns].mean()
# # plotcols = [greatest_differences(Xp, ppl.c3, 0, i).index[0] for i in [14,15,16]]
# plotcols = ['pca0','pca1','pca2']
# plot3d(Xp, ppl.c7, plotcols=plotcols,
#        centroids=centroids_p7, knames=c7_names)
#
# ########################################################################################
# ########################################################################################
# ### Cosine similarity of each person with each cluster center?
# ### to get how much each person corresponds to a particular type
# # from sklearn.metrics.pairwise import cosine_similarity
# #
# # sim = pd.DataFrame(cosine_similarity(Xp, centroids_p1.iloc[:, 1:]),
# #                    index=ppl.index, columns=centroids_p1.index)
