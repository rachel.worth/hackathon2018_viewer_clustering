
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as cm
from sklearn.preprocessing import scale
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from plot_fns import *
pd.set_option('display.width', 180)

from prep_for_clustering import *

# ### Choose version
# ### and take the dmas with the highest purchasing per person, enough to have this fraction of the total # ppl
# vers = 'Top25per'
# subsamp_frac = 0.25
#
# # vers = 'Program_all'
# # subsamp_frac = 1.
#
# print('Clustering {0} viewers, {1} of total sample'.format(vers, subsamp_frac))
# # from build_df import *
# ppl         = pd.read_csv('Program_all/person_data.csv'.format(vers), index_col = 0)
# ### Subsample?
# if subsamp_frac < 1.:
#     from dma_df import *
#     full_samp_size = purch_hh_dma.n_viewers.sum()
#     n=1
#     while purch_hh_dma.iloc[:n].n_viewers.sum() < full_samp_size * subsamp_frac:
#         n += 1
#     dma_list = purch_hh_dma.iloc[:n].index
#     ppl = ppl.loc[ppl['DMA CODE'].isin(dma_list)]
#     print('Using {0} of {1} people'.format(ppl.shape[0], full_samp_size))
# ### Identify different types of columns
# daypart_cols = ['night','morning','afternoon','evening']
# daypart_frac_cols = ['frac_'+c for c in daypart_cols]
# weekday_cols = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']
# weekday_frac_cols = ['frac_'+c for c in weekday_cols]
# genre_cols = [c for c in ppl.columns if c[:6] == 'genre_']
# genre_cols = [c for c in genre_cols if '_frac' not in c]
# genre_frac_cols   = ['frac_'+c for c in genre_cols]
# show_cols  = [c for c in ppl.columns if c[:2] == 'SH']
# movie_cols = [c for c in ppl.columns if c[:2] == 'MV']
# sp_cols    = [c for c in ppl.columns if c[:2] == 'SP']
# program_cols = show_cols + movie_cols + sp_cols
# program_frac_cols = ['frac_'+c for c in program_cols]
# tv_agg_cols = ['n_mins'] + daypart_cols + weekday_cols
# tv_cols     = tv_agg_cols + genre_frac_cols #+ program_cols
# ###
# program_info = pd.read_csv('data/show_info.csv', index_col = 0)[['program_id','program_name','show_type']]\
#                  .drop_duplicates().set_index('program_id')
# program_info['n_mins'] = ppl[program_cols].sum()
# program_info = program_info.sort_values('n_mins',ascending=False)
#
# ### Purchasing data
# purch_cols  = [c for c in ppl.columns if 'buy_' in c]
# ppl['hh_spent'] = ppl[purch_cols].sum(axis=1)
# dept_totals = ppl[purch_cols].sum().sort_values(ascending=False)
# buy_cols = list(dept_totals.index)
# frac_buy_cols = ['frac_'+c for c in buy_cols]
# ppl[frac_buy_cols] = ppl[buy_cols].divide(ppl[buy_cols].sum(axis=1), axis=0)
#
# ### Make numeric demographic columns
# brth_century = {9: 1800,
#                 0: 1900,
#                 1: 2000}
# race_dict = {1:  'White',
#              2:  'Black',
#              3:  'Japanese',
#              4:  'Chinese',
#              5:  'Filipino',
#              6:  'Korean',
#              7:  'Vietnamese',
#              8:  'American Indian',
#              9:  'Asian Indian',
#              10: 'Hawaiian',
#              11: 'Other',
#              99: 'Unknown'}
# lang_dict   = {0: 0,
#                1: 1,
#                2: 0,
#                3: 0.75,
#                4: 0.25,
#                5: 0.5,
#                6: 0}
# origin_dict = {1: 'Non-Spanish',
#                2: 'Spanish',
#                9: 'Unknown'}
# ### Already numeric?
# demos = ppl[['INCOME AMT','HOH EDUCATION CODE','Education','Number of Working Hours', 'TIME ZONE CODE']].copy()
# ### Convert weird Nielsen things
# ### Age as of Jan 1, 2017
# demos['age'] = 2017 - (pd.Series([brth_century[i] for i in ppl['Century of Birth']], index = ppl.index) + \
#                        pd.Series([int(s[:2]) for s in ppl['Birth Date'].astype('str').str.zfill(4)], index = ppl.index))
# demos['amt_spanish_spoken'] = [lang_dict[i] for i in ppl["Person's Language"]]
# demos['spanish_origin']     = ppl["Person's Origin"] == 2
# demos['is_male'] = ppl['Sex'] == 'M'
# for i in ppl['Race'].value_counts().index:
#     demos['is_'+race_dict[i]] = ppl['Race'] == i
# # for c in ['METRO CNTY IND','BOTTLED WATER IND','COFFEE TEA IND','SOFT DRINKS IND','TABLE WINE IND']:
# #     demos[c] = (ppl[c] == 'Y')
# demos['metro']      = ppl[   'METRO CNTY IND'] == 'Y'
# demos['btld_water'] = ppl['BOTTLED WATER IND'] == 'Y'
# demos['coffee_tea'] = ppl[   'COFFEE TEA IND'] == 'Y'
# demos['soda']       = ppl[  'SOFT DRINKS IND'] == 'Y'
# demos['wine']       = ppl[   'TABLE WINE IND'] == 'Y'
# demos['has_dog']    = ppl['NUM OF DOGS'] > 0
# demos['has_cat']    = ppl['NUM OF CATS'] > 0
# ### Household ages
# # for c in ['NUM OF ADULTS', 'NUM OF CHILDREN UNDER 18', 'NUM OF CHILDREN UNDER 12', 'NUM OF CHILDREN UNDER 3']:
# #     demos[c] = ppl[c]
# demos['n_adults'] = ppl['NUM OF ADULTS']
# demos['n_12-18']  = ppl['NUM OF CHILDREN UNDER 18'] - ppl['NUM OF CHILDREN UNDER 12']
# demos['n_6-12']   = ppl['NUM OF CHILDREN UNDER 12'] - ppl['NUM OF CHILDREN UNDER 6']
# demos['n_3-6']    = ppl['NUM OF CHILDREN UNDER 6']  - ppl['NUM OF CHILDREN UNDER 3']
# demos['n_0-3']    = ppl['NUM OF CHILDREN UNDER 3']
# demo_cols = list(demos.columns)
# ppl[demo_cols] = demos
#
# ### Fit clusters on the following columns
# fit_cols = ['frac_genre_'+str(n) for n in ['999','919','417']] + ['frac_'+c for c in program_info.index[:1000]] #program_cols
# X = pd.DataFrame(scale(ppl[fit_cols]), columns = fit_cols, index = ppl.index)
# ### Transform data to percentiles
# X_per = X.rank(pct=True, axis=0) - 0.5
#
############################################################################################
### Perform principal component analysis to see how much redundancy there is in the data
pca = PCA()
Xp = pca.fit_transform(X)
pca_components = pd.DataFrame(pca.components_, columns = fit_cols).transpose()
pca_components['names'] = list(program_info.loc[pca_components.index.str.replace('frac_', ''), 'program_name'])
# to see elements of a component: pca_components[[0, 'names']].sort_values(0)
### See how much variance was explained by each of the new components
plt.clf()
plt.scatter(range(Xp.shape[1]), np.log(pca.explained_variance_ratio_));
plt.xlabel('Parameter number', fontsize=20);
plt.ylabel('log(explained variance ratio)', fontsize=20);
plt.savefig('{0}/pca_evr.png'.format(vers))
plt.close('all')
### Reduce X by dropping lowest-importance variable
nvars = np.sum(np.log(pca.explained_variance_ratio_) > -30)
Xr = Xp[:,:nvars]
print(Xp.shape, Xr.shape)

########################################################################################
### Fit models with a range of k (number of clusters)
clusters = {}
klist = np.arange(2, 20)
for k in klist:
    print(k)
    clusters[k] = KMeans(n_clusters = k, random_state=32)
    clusters[k].fit(Xr)

### Get the inertia (resitual error) for each model
inertias = [clusters[k].inertia_ for k in klist]
plt.clf()
plt.scatter(klist, inertias/klist)
plt.xlabel('# of Clusters', fontsize=20);
plt.ylabel('Inertia/clusters', fontsize=20);
plt.savefig('{0}/cluster_inertia.png'.format(vers))
plt.close('all')

di = [(inertias[i] - inertias[i+1])/inertias[i] for i in range(len(inertias)-1)]
plt.clf()
plt.scatter(klist[1:], di);
plt.xlabel('# of clusters', fontsize=20);
plt.ylabel('Relative decrease in Inertia', fontsize=20);
plt.savefig('{0}/cluster_delta_inertia.png'.format(vers))
plt.close('all')

###################################################################################################
####################################### Plots of clusters #########################################
###################################################################################################

for k in np.arange(2,20):
    plotClusters(ppl, X, X_per, Xr, clusters, k, vers, 100,
                 genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols,
                 program_info, )

### cluster summary for each cluster of a given run
sum_18 = summarize_clusters(ppl, 18, demo_cols)

### add top 5 shows for each?
ProgramComparisons = getComparison(ppl[list(pd.Series(program_info.index).dropna()) +
                                       ['cluster_18']], agg='mean', clustercol='cluster_18')
top_show_cols = ['show'+str(i) for i in range(1, 6)]
for c in top_show_cols:
    sum_18.loc[c] = np.nan
for c in sum_18:
    sum_18.loc[top_show_cols, c] = list(program_info.loc[
                                            ProgramComparisons.loc[c].sort_values(ascending=False).dropna().iloc[:5].index,
                                            'program_name'])

sum_18.columns = ['mainstream', 'right', 'left', 'kids_older', 'kids_younger', 'crimedrama']
with open('{0}/{1}_summary.csv'.format(vers, 18), 'w') as f:
    f.write(sum_18.transpose().to_string())

### Most popular for a given cluster?
ProgramComparisons = getComparison(ppl[list(pd.Series(program_info.index).dropna()) +
                                       ['cluster_19']], agg='mean', clustercol='cluster_19')
k19_1 = pd.DataFrame(ProgramComparisons.loc[1].sort_values(ascending=False))
k19_1.columns = ['preference']
k19_1['name'] = program_info.loc[ProgramComparisons.loc[1].sort_values(ascending=False).index, 'program_name']
k19_1['total_n_mins'] = program_info.loc[ProgramComparisons.loc[1].sort_values(ascending=False).index, 'n_mins']

program_info['k19_1_preference'] = ProgramComparisons.loc[1]
program_info['k19_2_preference'] = ProgramComparisons.loc[2]

### Examine 3D plot?
k=19

suf = '_'+str(k)
n_members = ppl['cluster' + suf].value_counts()
colors = cm.rainbow(np.linspace(0, 1, k))
f2 = plt.figure(figsize=[8, 8])
ax2 = f2.add_subplot(111, projection='3d')
for i in n_members.index:
    ax2.scatter(Xr[np.array(X['cluster' + suf] == i), 0],
                Xr[np.array(X['cluster' + suf] == i), 1],
                Xr[np.array(X['cluster' + suf] == i), 2],
                color=colors[i], alpha=0.25, marker='o', label='k{0}, n={1}'.format(i, n_members.loc[i]));
leg = ax2.legend()
for lh in leg.legendHandles:
    lh.set_alpha(1)
ax2.set_title('Clusters over first three PCs', fontsize=20);
ax2.legend()

########################################################################################################################
#  plotClusters(ppl, X, X_per, Xr, clusters, 3, vers,
#              genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols)
#
# plotClusters(ppl, X, X_per, Xr, clusters, 8, vers,
#              genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols)
#
# plotClusters(ppl, X, X_per, Xr, clusters, 10, vers,
#              genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols)
#
# plotClusters(ppl, X, X_per, Xr, clusters, 13, vers,
#              genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols)
#
#
# #
# # genre_col_codes = [l.split('_')[-1] for l in [c for c in X.columns if 'genre_' in c]]
# # genre_col_descr = [sttn_genre.get_value(c, 'description') for c in genre_col_codes]
# # genre_col_label = ['{0} ({1})'.format(genre_col_descr[i], genre_col_codes[i]) for i in range(len(genre_col_codes))]
# # suf='_good'
# # from mpl_toolkits.mplot3d import Axes3D
# # import matplotlib.cm as cm
# # k=4
# # # colors = cm.rainbow(np.linspace(0, 1, k))
# # colors=['red','orange','green','blue']
# # X['cluster'] = clusters[k].labels_
# # ppl['cluster'] = X.cluster
# # # ### Let's see how many people are in each cluster
# # ClustersBySize = X.cluster.value_counts()
# # ### Renumber in order of size, to make the cluster names more meaningful.
# # Relabel = {}
# # for i, c in enumerate(ClustersBySize.index):
# #     Relabel[c] = i
# # X['cluster'] = [Relabel[c] for c in X.cluster]
# #
# # ### Plot tv columns
# # clustercolComparisons = getComparison(ppl[tv_cols + ['cluster']])
# # RelPlot(clustercolComparisons.iloc[:,:19], 'Viewer Segments vs TV columns',
# #         xlab=(tv_agg_cols + genre_col_label)[:19], RelMax=1.5)
# # plt.tight_layout()
# # plt.savefig('{0}/cluster_tv{1}.png'.format(vers, suf))
# # plt.close('all')
# #
# # ### Viewer demographic info by cluster
# # DemoComparisons = getComparison(ppl[demo_cols + ['cluster']], agg='mean')
# # RelPlot(DemoComparisons, 'Viewer Cluster Demographics')
# # plt.tight_layout()
# # plt.savefig('{0}/cluster_demos{0}.png'.format(vers, suf))
# # plt.close('all')
# #
# # ### 3D plot, leave open
# # f2 = plt.figure(figsize=[8, 8])
# # ax2 = f2.add_subplot(111, projection='3d')
# # for i in range(k):
# #     ax2.scatter(Xr[np.array(X.cluster==i),0], Xr[np.array(X.cluster==i),1], Xr[np.array(X.cluster==i),2],
# #                 color=colors[i], alpha=0.25,  marker='o', label='k'+str(i));
# # ax2.legend(loc='best');
# # ax2.set_title('Clusters over first three PCs', fontsize=20);
# # ax2.legend()
