print('Run read_tuning')
from read_tuning import *

########################################################################################################################
### This script creates a dataframe with one row for each viewer, with some aggregate columns drawn from tuning data

viewer_cols = ['hh_prsn', 'Household Number', 'Person Number',
               'DMA CODE', 'HHLD ZIP CODE', 'TIME ZONE CODE',
               'Birth Date', 'Century of Birth', 'Sex', 'Race',
               'Education', 'HOH EDUCATION CODE',
               'Occupation','Nielsen Occupation Code','Number of Working Hours','Work Out- side Home',
               "Person's Language", "Person's Origin",
               'METRO CNTY IND', 'INCOME AMT',
               'NUM OF ADULTS', 'NUM OF CHILDREN UNDER 18', 'NUM OF CHILDREN UNDER 12',
               'NUM OF CHILDREN UNDER 6', 'NUM OF CHILDREN UNDER 3', 'NUM OF CHILDREN UNDER 2',
               'NUM OF CATS', 'NUM OF DOGS',
               'BOTTLED WATER IND','COFFEE TEA IND','SOFT DRINKS IND','TABLE WINE IND']

### Aggregate columns from tuning data, by person
# print('Perform aggregations')
# aggregations = {
#     'Person Number': {'n_views': 'count'},
#     'Duration': {'drtn_sum':'sum',
#                  'drtn_mn': 'mean'}
#                 }
#
# tuning_aggs = tuning.groupby(['hh_prsn']).agg(aggregations)
# tuning_aggs.columns = tuning_aggs.columns.droplevel(0)
# ### Viewing by part of day
# tuning_aggs['frac_night'] = tuning.loc[(tuning['Start Minute'] >= 60*0) &
#                                        (tuning['Start Minute'] <  60*6)]\
#                                   .groupby(['hh_prsn'])['Duration'].sum() / tuning_aggs.drtn_sum
# tuning_aggs['frac_morn']  = tuning.loc[(tuning['Start Minute'] >= 60*6) &
#                                        (tuning['Start Minute'] <  60*12)]\
#                                   .groupby(['hh_prsn'])['Duration'].sum() / tuning_aggs.drtn_sum
# tuning_aggs['frac_aftn']  = tuning.loc[(tuning['Start Minute'] >= 60*12) &
#                                        (tuning['Start Minute'] <  60*18)]\
#                                   .groupby(['hh_prsn'])['Duration'].sum() / tuning_aggs.drtn_sum
# tuning_aggs['frac_evng']  = tuning.loc[(tuning['Start Minute'] >= 60*18) &
#                                        (tuning['Start Minute'] <= 60*24)]\
#                                   .groupby(['hh_prsn'])['Duration'].sum() / tuning_aggs.drtn_sum
# # tuning_aggs['primetime_drtn_sum'] = tuning\
# #     .loc[(tuning['Start Minute'] >= 60*8) & (tuning['Start Minute'] <= 60*11)]\
# #     .groupby(['hh_prsn'])['Duration'].sum()
# tuning_aggs.fillna({'frac_night': 0,
#                     'frac_morn':  0,
#                     'frac_aftn':  0,
#                     'frac_evng':  0},
#                    inplace=True);
#
# ### More complicated aggregations
# print('Perform pivots')
# genre_sums = pd.pivot_table(data = tuning, index = 'hh_prsn', columns='genre_code',
#                values='Duration', aggfunc = np.sum, fill_value=0)
# genre_sums = genre_sums[genre_sums.sum().sort_values(ascending=False).index[:30]]
# # genre_frac = genre_sums.divide(genre_sums.sum(axis=1), axis=0).fillna(0)
# ### Separate out nonhut (999) and dvr play (919)
# tuning_aggs[['frac_nonhut','frac_dvr','frac_vod']] = genre_sums[['999','919','417']].divide(genre_sums.sum(axis=1), axis=0)
# tuning_aggs.fillna({'frac_nonhut': 0,
#                     'frac_dvr':    0,
#                     'frac_vod':    0},
#                    inplace=True);
# genre_frac = genre_sums.drop(['999','919','417'], axis=1).divide(
#              genre_sums.drop(['999','919','417'], axis=1).sum(axis=1), axis=0).fillna(0)
#
# ### Rename columns to make sense after joining
# # genre_sums.columns = [ 'sum_genre_'+c for c in genre_sums.columns]
# genre_frac.columns = [c if c not in ["{:1d}".format(x) for x in range(10)] else c.zfill(2) for c in genre_frac.columns]
# genre_frac.columns = ['genre_'+c for c in genre_frac.columns]

purch_by_dept.columns = ['buy_'+c for c in purch_by_dept.columns]

### Convert from counts to fractions??



### Join demographic and tuning data
### Set hh/person id as index, remove people with no tuning data
print('Merge and write data')
assert viewers.shape[0] == len(viewers.hh_prsn.unique()), 'ERROR: hh_prsn is not a unique identifier'
ppl = viewers.loc[viewers['hh_prsn'].isin(tuning_aggs.index), viewer_cols].set_index('hh_prsn')
ppl = ppl.merge(purch_by_dept, left_on='Household Number', right_index=True, how='left')\
         .join(tuning_aggs,   how='left')
         # .join(genre_frac,    how='left')

### data/person_data.csv is the one that's used
vers = 'Program_all'
ppl.to_csv('{0}/person_data.csv'.format(vers))
