import pandas as pd
pd.set_option('display.width', 180)
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sb
from sklearn.decomposition import NMF

from functions import *
from prep_for_clustering import *

### Tag
vers = 'nmf'
### Save plots to this directory
dir  = 'nmf_frac/'

### Number of genres/clusters to create
nk=20

### Columns to fit model on
fit_cols = ['frac_'+c for c in program_info.index]
### Exclude the broad genres? they're not very informative
fit_cols = [f for f in fit_cols if 'genre_' not in f]

### Drop people with no tuning
ppl = ppl.loc[ppl[fit_cols].sum(axis=1) > 0].copy()
print(str(ppl.shape[0])+' people with tuning in the data')

### Matrix of 'ratings' (i.e. viewing fraction)
R = ppl[fit_cols].copy()
print(R.shape)

### Cross validate? (doesn't seem to matter)
cv = 2
nmf_list = []
nmf_scores = []
for i in range(cv):
    print('NMF run {0} out of {1}'.format(i, cv))
    nmf_list.append(NMF(n_components=nk, max_iter=200))
    nmf_list[i].fit(R)
    nmf_scores.append(nmf_list[i].reconstruction_err_)
print(nmf_scores)
use_i = nmf_scores.index(min(nmf_scores))
nmf = nmf_list[use_i]

### User and program matrices
U = pd.DataFrame(nmf.transform(R), index=R.index)
P = pd.DataFrame(nmf.components_, columns=R.columns)

### Evaluate?
### How well does it predict viewing?
nR = U.dot(P)
print(rms(R-nR, axis=1).describe())

### Assign names based on top show per genre?
P2 = P.copy()
P2.columns = [program_info.loc[i.replace('frac_',''), 'program_name'] for i in P.columns]

rename_k = {}
for k in P2.index:
    rename_k[k] = 'k{0}_{1}'.format(k, P2.loc[k].sort_values().index[-1])
P2 = P2.rename(index=rename_k)
U2 = U.rename(columns=rename_k)

### Get each person's top k, use that as "cluster"?
cn = 'c1'
ppl['c1'] = U.idxmax(axis=1)

### Assign names based on top shows
### Compared with files in new_names directory
sums = summarize_clusters(ppl, 'c1', demo_cols, program_info)
shows = summarize_clusters(ppl, 'c1', demo_cols, program_info, n_shows=15).loc[['show'+str(i) for i in range(1,16)]]
c_names, name_score = name_clusters(shows)

### creating a new cluster name file
# shows[24].drop_duplicates().to_csv('new_names/unclassifiable.csv')

ppl['c3'] = [c_names.loc[i,'name'] for i in ppl.c1]
cn = 'c3'

### Plot with previous plotting functions
print('Creating heatmaps')
plotClusters(ppl, ppl[cn],
             sttn_genre, genre_cols, tv_agg_cols, buy_cols, frac_buy_cols, demo_cols, program_info,
             pre=dir+'{0}_'.format(nk), klist=ppl[cn].sort_values().unique())

### 3D scatterplot over the first three "genre" dimensions
U3 = U.copy()
U3.columns = [c_names.loc[c, 'name'] for c in U3.columns]

for i, plot3d_cols in enumerate([U3.columns[:3],
                                 ['newsFOX','newsCNN','newsMSNBC'],
                                 [c for c in c_names.name if 'kids' in c][:3]]):
    f2, ax2 = plot3d(U3, ppl[cn], plotcols=plot3d_cols)
    f2.savefig(dir+'/{0}_3D_{1}.png'.format(nk, ['first','news','kids'][i]))
    # plt.close(f2) # or keep open to be able to rotate?

### For each cluster, what is its members' average association with the others?
### User similarity
member_sim = pd.DataFrame([], index=ppl.c1.unique(), columns=ppl.c1.unique())
for c in member_sim.columns:
    member_sim[c] = U.loc[ppl.c1==c].mean()
member_sim = member_sim.rename(index=c_names.name, columns=c_names.name)
member_sim = member_sim.sort_index()
member_sim = member_sim[member_sim.index]

### Program similarity
program_sim = pd.DataFrame([], index=ppl.c1.unique(), columns=ppl.c1.unique())
for c in program_sim.columns:
    program_sim[c] = P.loc[:, P.idxmax(axis=0)==c].mean(axis=1)
program_sim = program_sim.rename(index=c_names.name, columns=c_names.name)
program_sim = program_sim.sort_index()
program_sim = program_sim[program_sim.index]

f1, ax1 = plt.subplots(1,2, figsize=(22,11))
for i, d in enumerate([member_sim, program_sim]):
    sb.heatmap(d,  square=True, ax=ax1[i], cbar=False, linewidths=0.2)
    ax1[i].set_xticklabels(ax1[i].get_xticklabels(), rotation=30, ha='right', va='top')
    ax1[i].set_yticklabels(ax1[i].get_yticklabels(), rotation=30, ha='right', va='top')
    ax1[i].set_title(['Users','Programs'][i])
f1.text(0.5, 0.97, "Column's members' mean belongingness to row's genre", fontsize=15, ha='center')
plt.tight_layout()
f1.savefig(dir+'{0}_similarities.png'.format(nk))
plt.close(f1)



