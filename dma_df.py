import pandas as pd

########################################################################################################################
### DMA info
print('Read dma data')
dmas = pd.read_csv('data/DMA_info.csv', index_col = 0)
dmas = dmas.loc[[s for s in dmas.index if '-' not in s]]
dmas.index = dmas.index.astype('int')

########################################################################################################################
### Person-level file
### Needs b1010, then tgb2 formatting
print('Read viewer data')
from file_formatting import *
viewers = pd.read_fwf('data/b1010_tgb2.oct16.unified', skipinitialspace = False,
                      header=None, names = char_format['title'],
                      colspecs = list(char_format[['range_start','range_end']].itertuples(index=False)))
viewers['hh_prsn']  = viewers['Household Number'].astype('str') + '-' + viewers['Person Number'].astype('str')

########################################################################################################################
### Purchasing history data
print('Read purchasing data')

purchasing = pd.read_csv('data/purch_data.csv')
### How many months does each hh have spending in?
purchasing['month'] = [s[2:5] for s in purchasing.CAL_DT]
hh_n_months = purchasing[['PANL_HHLD_ID_NBR','month']].drop_duplicates().groupby('PANL_HHLD_ID_NBR').month.count()
ok_hhs = hh_n_months.loc[hh_n_months >= 10].index
purchasing['n_mnths'] = list(hh_n_months.loc[purchasing['PANL_HHLD_ID_NBR']])
purchasing['covg'] = purchasing['n_mnths'] >= 10

### Total spending per hh per dept
purch_by_dept = pd.pivot_table(data = purchasing, index = 'PANL_HHLD_ID_NBR', columns = 'DEPT_DESCR',
                               values='DOLLARS_SPENT', aggfunc = 'sum', fill_value = 0)
# purch_by_dept.to_csv('data/purch_hhs.txt', columns = [], header=False)

########################################################################################################################
### Fraction of houses covered, by dma
dma_purch_covg = purchasing[['PANL_HHLD_ID_NBR','covg']].drop_duplicates()\
                           .merge(viewers[['Household Number', 'DMA CODE', 'NUM OF PERSONS']],
                                  left_on='PANL_HHLD_ID_NBR', right_on='Household Number', how='left')\
                           .groupby(['DMA CODE']).agg({'covg':'mean',
                                                       'NUM OF PERSONS':'sum'})\
                           .sort_values('covg', ascending=False)

### Find avg hh spending by dma
print('Create purchasing/dma table')
purch_hh = purchasing.merge(viewers[['Household Number','NUM OF PERSONS','DMA CODE']],
                            left_on='PANL_HHLD_ID_NBR', right_on='Household Number', how='left')\
                     .groupby(['PANL_HHLD_ID_NBR','NUM OF PERSONS','DMA CODE'])['DOLLARS_SPENT'].sum().reset_index()
purch_hh['purch_per_prsn'] = purch_hh['DOLLARS_SPENT']/purch_hh['NUM OF PERSONS']

purch_hh_dma = pd.DataFrame(purch_hh.groupby('DMA CODE')['DOLLARS_SPENT'].mean().sort_values(ascending=False))
purch_hh_dma.columns = ['purch_per_hh']
purch_hh_dma['n_viewers'] = viewers.groupby('DMA CODE')['hh_prsn'].count()
purch_hh_dma = purch_hh_dma.join(dmas[['name','state']], how='left')

purch_hh_state = purch_hh_dma.groupby('state').purch_per_hh.median().sort_values()
########################################################################################################################
dma_stats = purch_hh_dma.join(dma_purch_covg, how='outer')
dma_stats.columns = ['purch_per_hh','n_viewers','name','state','n_buyers','coverage_10mo']
dma_stats = dma_stats[['name','state','coverage_10mo','purch_per_hh','n_viewers','n_buyers']]
dma_stats['n_ppl_min'] = dma_stats[['n_viewers','n_buyers']].min(axis=1)

# plt.plot(dma_stats.loc[dma_stats.purch_per_hh> 1000, 'coverage_10mo'],
#          dma_stats.loc[dma_stats.purch_per_hh> 1000, 'purch_per_hh'], 'bo')
# plt.plot(dma_stats.loc[dma_stats.purch_per_hh<=1000, 'coverage_10mo'],
#          dma_stats.loc[dma_stats.purch_per_hh<=1000, 'purch_per_hh'], 'ro')